<?php

namespace Database\Seeders;


use App\Models\Permission;
use App\Models\PermissionGroup;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create roles and assign existing permissions
        Role::firstOrCreate(['name' => 'Super Admin','guard_name'=>'web','level'=>0]);

        $role = Role::firstOrCreate(['name' => 'Admin','guard_name'=>'web','level'=>1]);

        $group= PermissionGroup::firstOrCreate(['name' => 'Users']);
        Permission::firstOrCreate(['name' => 'users index','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'users create','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'users edit','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'users delete','permission_group_id'=>$group->id,'guard_name'=>'web']);

        $group= PermissionGroup::firstOrCreate(['name' => 'Roles']);
        Permission::firstOrCreate(['name' => 'roles index','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'roles create','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'roles edit','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'roles delete','permission_group_id'=>$group->id,'guard_name'=>'web']);

        $group= PermissionGroup::firstOrCreate(['name' => 'Permission Groups']);
        Permission::firstOrCreate(['name' => 'permission groups index','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'permission groups create','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'permission groups edit','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'permission groups delete','permission_group_id'=>$group->id,'guard_name'=>'web']);


        $group= PermissionGroup::firstOrCreate(['name' => 'Permission']);
        Permission::firstOrCreate(['name' => 'permission index','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'permission create','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'permission edit','permission_group_id'=>$group->id,'guard_name'=>'web']);
        Permission::firstOrCreate(['name' => 'permission delete','permission_group_id'=>$group->id,'guard_name'=>'web']);



        $role->givePermissionTo(['users index','users create','users edit','users delete']);

        // Create super-admin user
        $user = User::firstOrCreate([
            'name' => 'super@cyntrek.com',
            'email' => 'super@cyntrek.com',
            'password' => Hash::make('super@cyntrek.com'),
        ]);

        $user->assignRole('Super Admin');

        // Create admin user
        $user = User::firstOrCreate([
            'name' => 'admin@cyntrek.com',
            'email' => 'admin@cyntrek.com',
            'password' => Hash::make('admin@cyntrek.com'),
        ]);

        $user->assignRole('Admin');


    }
}
