<?php

namespace App\Imports;


use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Row;

class QuestionImport implements OnEachRow, WithStartRow
{
    public $paper_id;

    /**
     * LecturerImport constructor.
     */
    public function __construct($paper_id)
    {
        $this->paper_id = $paper_id;
    }

    public function startRow(): int
    {
        return 2;
    }


    public function onRow(Row $row)
    {
        $total_count =$row->toCollection()->count();
        $answer_count =  $total_count- 2;


        $old_question = Question::wherePaperId($this->paper_id)->whereTitle($row[0])->exists();
        if (!$old_question) {
            $question = new Question();
            $question->title = $row[0];
            $question->marks = $row[1];
            $question->paper_id = $this->paper_id;
            $question->created_by = Auth::id();
            $question->save();


            $j =1;
            for ($i = 2; $i <= $answer_count; $i++) {
                $is_correct = $row[$total_count-1]==$j ?1:0;
                $answer = Answer::create(['title' => $row[$i],'question_id' => $question->id,'is_correct'=>$is_correct,'answer_order'=>$j, 'created_by' => Auth::id()]);
                $j++;
            }
        }


    }
}
