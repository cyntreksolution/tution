<?php

namespace App\Imports;


use App\Models\Student;
use App\Models\StudentEnroll;
use App\Models\TutionClass;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpsertColumns;
use Maatwebsite\Excel\Row;
use function Symfony\Component\String\s;

class StudentImport implements WithStartRow, OnEachRow
{
    public $class_id;

    /**
     * LecturerImport constructor.
     */
    public function __construct($class_id)
    {
        $this->class_id = $class_id;
    }

    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */


    public function onRow(Row $row)
    {
        $rowIndex = $row->getIndex();
        $row = $row->toArray();

        $group = TutionClass::find($this->class_id);

        $student_old = Student::whereName($row[0])->whereMobile($row[1])->first();

        if (!empty($student_old) && $student_old->count() > 0) {
            $flag =StudentEnroll::whereStudentId($student_old->id)->whereTutionClassId($this->class_id)->exists();
            if (!$flag) {
                $student_old->classes()->attach($this->class_id);
            }
        } else {
            $student = $group->students()->create([
                'name' => $row[0],
                'mobile' => $row[1],
                'email' => rand(9999, 99999),
                'password' => Hash::make('123456789'),
                'is_student' => 1,
            ]);

            $student->assignRole('Student');
        }


    }


}
