<?php

namespace App\Imports;


use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class LecturerImport implements ToModel,WithStartRow
{
    public $institute_id;
    /**
     * LecturerImport constructor.
     */
    public function __construct($institute)
    {
        $this->institute_id=$institute;
    }

    public function startRow(): int
    {
        return 2;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $lecture = new User([
            'name' => $row[0],
            'mobile' => $row[1],
            'email' => rand(9999,99999),
            'password' => Hash::make('123456789'),
            'institute_id' => $this->institute_id,
            'is_lecturer' => 1,
        ]);

        $lecture->assignRole('Lecturer');
        return $lecture;
    }


}
