<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaperClass extends Model
{
    use HasFactory;

    protected $table ='paper_class';

    protected $hidden=['created_at','updated_at','deleted_at'];

    public function paper(){
        return $this->belongsTo(Paper::class);
    }
}
