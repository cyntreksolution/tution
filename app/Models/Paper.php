<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Paper extends Model
{
    use HasFactory;
    use SoftDeletes;
    use LogsActivity;

    protected static $logName = 'papers';
    protected static $logAttributes = ['title', 'duration', 'status','marks','is_equal_marks','is_shuffle_questions','is_shuffle_answers','exam_time'];


    protected $fillable= ['title', 'duration', 'status','marks','is_equal_marks','is_shuffle_questions','is_shuffle_answers','exam_time'];

    public function classes(){
        return $this->belongsToMany(TutionClass::class,'paper_class');
    }

    public function questions(){
        return $this->hasMany(Question::class,'paper_id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('papers.*', 'tution_classes.name as class_name', 'tution_classes.id as class_id')
            ->join('paper_class', 'papers.id', '=', 'paper_class.paper_id')
            ->join('tution_classes', 'paper_class.tution_class_id', '=', 'tution_classes.id')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopeFilterData($query)
    {
        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('paper.id', 'like', "%" . $term . "%")
            ->orWhere('paper.title', 'like', "%" . $term . "%")
            ->orWhereHas('classes', function ($q) use ($term) {
                $q->where('name', 'like', "%" . $term . "%");
            })
            ->orWhere('marks', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('duration', 'like', "%" . $term . "%")
            ->orWhere('exam_time', 'like', "%" . $term . "%");
    }
}
