<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Institute extends Model
{
    use HasFactory;
    use SoftDeletes;
    use LogsActivity;

    protected static $logName = 'institute';
    protected static $logAttributes = ['name', 'telephone', 'address','owner_id'];
    protected $hidden=['created_at','updated_at','deleted_at'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function cities()
    {
        return $this->belongsToMany(City::class, 'institute_cities');
    }


    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
//            ->select('institutes.*', 'cities.name_en', 'users.name')
//            ->join('institute_cities', 'institute_cities.institute_id', '=', 'institutes.id')
//            ->join('cities', 'institute_cities.city_id', '=', 'cities.id')
//            ->join('users', 'institutes.owner_id', '=', 'users.id')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopeFilterData($query, $promotion, $channel, $outlet, $user, $date)
    {
        if (!empty($promotion)) {
            $query->where('id', '=', $promotion);
        }
        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('institutes.id', 'like', "%" . $term . "%")
            ->orWhere('institutes.name', 'like', "%" . $term . "%")
            ->orWhereHas('owner', function ($q) use ($term) {
                $q->where('name', 'like', "%" . $term . "%");
            })
            ->orWhere('address', 'like', "%" . $term . "%")
            ->orWhere('telephone', 'like', "%" . $term . "%")
            ->orWhereHas('cities', function ($q) use ($term) {
                $q->where('name_en', 'like', "%" . $term . "%");
            });
    }

}
