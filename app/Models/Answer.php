<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Answer extends Model
{
    use HasFactory;
    use SoftDeletes;
    use LogsActivity;

    protected static $logName = 'answers';
    protected static $logAttributes = ['question_id', 'title', 'is_correct', 'answer_order', 'created_by'];

    protected $fillable = ['question_id', 'title', 'is_correct', 'answer_order', 'created_by'];

    public function scopeCorrectAnswer($query)
    {
        return $query->where('is_correct', '=', 1);
    }
}
