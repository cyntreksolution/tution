<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Question extends Model
{
    use HasFactory;
    use SoftDeletes;
    use LogsActivity;

    protected static $logName = 'questions';
    protected static $logAttributes = ['title', 'marks', 'question_order', 'type ', 'created_by', 'paper_id'];

    protected $fillable = ['title', 'marks', 'question_order', 'type ', 'created_by', 'paper_id'];

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function scopeCorrectAnswer($query)
    {
        $query = $query->whereHas('answers', function ($q) {
            $q->where('is_correct', '=', 1);
        });
        return $query;
    }
}

