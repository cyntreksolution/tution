<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PaperEnroll extends Model
{
    use HasFactory;
    protected $table ='student_paper_enroll';

    public function paper(){
        return $this->belongsTo(Paper::class);
    }

}
