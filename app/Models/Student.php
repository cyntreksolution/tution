<?php

namespace App\Models;

use App\Scopes\LecturerScope;
use App\Scopes\StudentScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class Student extends Model
{
    use HasFactory;

    use HasFactory,SoftDeletes,LogsActivity,HasRoles;
    protected $table ='users';
    protected $guard_name = 'web';


    protected static $logName = 'student';
    protected static $logAttributes = ['name', 'email', 'password','mobile','institute_id','is_student','is_lecturer'];


    protected $hidden=['password','two_factor_secret','two_factor_recovery_codes','remember_token','current_team_id','created_at','updated_at','deleted_at'];

//
//    protected static function booted()
//    {
//        static::addGlobalScope(new StudentScope());
//    }

    public function classes()
    {
        return $this->belongsToMany(TutionClass::class,'student_enroll','student_id','tution_class_id');
    }



    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('users.*','tution_classes.name as class_name','tution_classes.id as class_id')
            ->leftJoin('student_enroll','student_enroll.student_id','=','users.id')
            ->leftJoin('tution_classes','student_enroll.tution_class_id','=','tution_classes.id')
            ->where('users.is_student','=',1)
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeFilterData($query, $class, $email, $name, $number, $date_range,$status)
    {
        if (!empty($class)) {
            $query->where('student_enroll.tution_class_id', '=', $class);
        }

        if (!empty($email)) {
            $query->where('email', 'like', "%$email%");
        }
        if (!empty($name)) {
            $query->where('users.name', 'like', "%$name%");
        }
        if (!empty($number)) {
            $query->where('mobile', 'like', "%$number%");
        }

        if (!empty($date_range)) {
            $date = explode(' - ', $date_range);
            $start_date = $date[0];
            $end_date = $date[1];
            $query->where('users.created_at','>=',$start_date)->where('users.created_at','<=',$end_date);
        }
        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('users.id', 'like', "%$term%")
            ->orWhere('users.name', 'like', "%$term% ")
            ->orWhere('users.email', 'like', "%$term%")
            ->orWhere('users.mobile', 'like', "%$term%");
    }
}
