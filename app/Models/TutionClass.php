<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class TutionClass extends Model
{
    use HasFactory;
    use SoftDeletes;
    use LogsActivity;

    protected static $logName = 'tution-class';
    protected static $logAttributes = ['name', 'institute_id', 'grade','academic_year','subject_id','user_id'];

    protected $fillable =['name', 'institute_id', 'grade','academic_year','subject_id','user_id'];
    protected $hidden=['created_at','updated_at','deleted_at'];

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function lecturer()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function students()
    {
        return $this->belongsToMany(User::class,'student_enroll','tution_class_id','student_id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('tution_classes.*', 'institutes.name as institute_name', 'subjects.name as subject_name','users.name as lecturer_name')
            ->leftJoin('institutes', 'tution_classes.institute_id', '=', 'institutes.id')
            ->leftJoin('subjects', 'tution_classes.subject_id', '=', 'subjects.id')
            ->leftJoin('users', 'tution_classes.user_id', '=', 'users.id')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('tution_classes.id', 'like', "%" . $term . "%")
            ->orWhere('tution_classes.name', 'like', "%" . $term . "%")
            ->orWhereHas('institute', function ($q) use ($term) {
                $q->where('name', 'like', "%" . $term . "%");
            })
            ->orWhere('grade', 'like', "%" . $term . "%")
            ->orWhere('academic_year', 'like', "%" . $term . "%")
            ->orWhereHas('subject', function ($q) use ($term) {
                $q->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('lecturer', function ($q) use ($term) {
                $q->where('name', 'like', "%" . $term . "%");
            });
    }
}
