<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermissionGroup extends Model
{
    use HasFactory,LogsActivity;

    protected static $logName = 'users';
    protected static $logAttributes = ['name'];

    protected $fillable = [
        'name',
    ];

    function permissions() {

        return $this->hasMany(Permission::class);
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('permission_groups.*')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopeFilterData($query, $promotion, $channel, $outlet, $user, $date)
    {
        if (!empty($promotion)) {
            $query->where('id', '=', $promotion);
        }
        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('permission_groups.id', 'like', "%" . $term . "%")
            ->orWhere('permission_groups.name', 'like', "%" . $term . "%");
    }
}
