<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;
    use LogsActivity;

    protected static $logName = 'users';
    protected static $logAttributes = ['name', 'email', 'password','mobile','institute_id','is_student','is_lecturer'];
    protected $hidden=['password','two_factor_secret','two_factor_recovery_codes','remember_token','current_team_id','created_at','updated_at','deleted_at'];

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }

    public function classes()
    {
        return $this->belongsToMany(TutionClass::class,'student_enroll','student_id','id','tution_class_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile','institute_id','is_student','is_lecturer'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**ma
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('users.*','institutes.name as institute_name')
            ->leftJoin('institutes','institutes.id','=','users.institute_id')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeFilterData($query, $institute, $email, $name, $number, $date_range,$status)
    {
        if (!empty($institute)) {
            $query->where('users.institute', 'like', "%$institute%");
        }

        if (!empty($email)) {
            $query->where('email', 'like', "%$email%");
        }
        if (!empty($name)) {
            $query->where('users.name', 'like', "%$name%");
        }
        if (!empty($number)) {
            $query->where('mobile', 'like', "%$number%");
        }

        if (!empty($date_range)) {
            $date = explode(' - ', $date_range);
            $start_date = $date[0];
            $end_date = $date[1];
            $query->where('users.created_at','>=',$start_date)->where('users.created_at','<=',$end_date);
        }

        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('users.id', 'like', "%$term%")
            ->orWhere('users.name', 'like', "%$term% ")
            ->orWhere('users.email', 'like', "%$term%")
            ->orWhere('users.mobile', 'like', "%$term%");
    }


}
