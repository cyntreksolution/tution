<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Subject extends Model
{
    use HasFactory;

    use SoftDeletes;
    use LogsActivity;

    protected static $logName = 'subjects';
    protected static $logAttributes = ['name'];

    protected $fillable =['name'];
}
