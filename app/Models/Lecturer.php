<?php

namespace App\Models;

use App\Scopes\LecturerScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\AssignOp\Mod;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class Lecturer extends Model
{
    use HasFactory,SoftDeletes,LogsActivity,HasRoles;
    protected $table ='users';
    protected $guard_name = 'web';


    protected static $logName = 'lecturer';
    protected static $logAttributes = ['name', 'email', 'password','mobile','institute_id','is_student','is_lecturer'];

    protected $hidden=['password','two_factor_secret','two_factor_recovery_codes','remember_token','current_team_id','created_at','updated_at','deleted_at'];



    protected static function booted()
    {
        static::addGlobalScope(new LecturerScope());
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('users.*','institutes.name as institute_name')
            ->leftJoin('institutes','institutes.id','=','users.institute_id')
            ->where('users.is_lecturer','=',1)
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeFilterData($query, $institute, $email, $name, $number, $date_range,$status)
    {
        if (!empty($institute)) {
            $query->where('users.institute_id', 'like', "%$institute%");
        }

        if (!empty($email)) {
            $query->where('email', 'like', "%$email%");
        }
        if (!empty($name)) {
            $query->where('users.name', 'like', "%$name%");
        }
        if (!empty($number)) {
            $query->where('mobile', 'like', "%$number%");
        }

        if (!empty($date_range)) {
            $date = explode(' - ', $date_range);
            $start_date = $date[0];
            $end_date = $date[1];
            $query->where('users.created_at','>=',$start_date)->where('users.created_at','<=',$end_date);
        }

        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('users.id', 'like', "%$term%")
            ->orWhere('users.name', 'like', "%$term% ")
            ->orWhere('users.email', 'like', "%$term%")
            ->orWhere('users.mobile', 'like', "%$term%");
    }

}
