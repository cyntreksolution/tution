<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class StudentEnroll extends Model
{
    use HasFactory;
    use HasFactory,SoftDeletes,LogsActivity,HasRoles;
    protected $table ='student_enroll';

    protected static $logName = 'student-enroll';
    protected static $logAttributes = ['student_id', 'tution_class_id'];


}
