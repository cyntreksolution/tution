<?php

namespace App\Exports;

use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersExport implements FromCollection,WithHeadings,WithMapping
{
    use Exportable;

    protected $rows;

    public function __construct($rows)
    {
        $this->rows = $rows;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->rows;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Registration Date',
            'Name',
            'Institute',
            'Mobile',
            'Email',
        ];
    }

    public function map($row): array{
        return [
            $row->id,
            Carbon::parse($row->created_at)->format('Y-m-d H:i'),
            $row->name,
            optional($row->institute)->name,
            $row->mobile,
            $row->email,
        ];
    }
}
