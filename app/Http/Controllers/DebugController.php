<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class DebugController extends Controller
{
    public function index()
    {
      $q = Question::find(19);
      dd($q->answers);
    }
}
