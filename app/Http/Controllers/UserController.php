<?php

namespace App\Http\Controllers;

use App\Exports\FullSummaryExport;
use App\Exports\UsersExport;
use App\Models\Business;
use App\Models\BusinessSection;
use App\Models\Institute;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id', 'DESC');
        $roles = Role::pluck('name', 'name');

        $institute = Institute::pluck('name', 'id')->all();
        return view('users.index', compact('data', 'roles', 'institute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirmpassword',
            'roles' => 'required',
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        $msg = 'User Created Successfully';
        return $this->sendResponse($user, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);


        $user->name = $request->name ;
        $user->mobile = $request->mobile;
        $user->institute_id = $request->institute_id;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path() . '/user_profile/';
            $fileName = $user->id . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $fileName);
            $user->profile_photo_path = '/user_profile/' . $fileName;
        }

        $user->syncRoles([$request->input('roles')]);

        $user->save();
        return redirect(route('user.profile', $user->id));

    }

    public function updateByAdmin(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);


        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->institute_id = $request->institute_id;

        $user->syncRoles([$request->input('roles')]);

        $user->save();
        return $this->sendResponse('', 'User Successfully Updated');
    }

    function updatePassword(Request $request, User $user)
    {
        $rules = array(
            'current_password' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed', 'different:current_password'],
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err_str = '';
            foreach ($errors as $error) {
                $err_str .= $error;
            }

            $request->session()->flash('alert-type', 'error');
            $request->session()->flash('message', $err_str);
            return redirect(route('user.profile', $user->id));
        }

        if (Hash::check($request->current_password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();

            $request->session()->flash('alert-type', 'success');
            $request->session()->flash('message', 'Password Changed Successfully');
            return redirect(route('user.profile', $user->id));

        } else {
            $request->session()->flash('alert-type', 'error');
            $request->session()->flash('message', 'Password does not match');
            return redirect(route('user.profile', $user->id));
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return $this->sendResponse('', 'User Successfully Deleted');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id','name', 'email','mobile','institute_name'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = User::query();
        $filter_count =0;
        $channels_count =User::all()->count();
        if (!empty($request->filter)) {
            $name = $request->name;
            $email = $request->email;
            $number = $request->number;
            $institute = $request->institute_id;
            $date_range = $request->registration_date;
            $status = $request->status;

            $dataset = $dataset->filterData($institute,$email,$name,$number,$date_range,$status);
            $filter_count = $dataset->count();
        }

        if (!is_null($search) || !empty($search)) {
            $dataset = $dataset->searchData($search);
            $channels_count = $dataset->count();
        }

        $dataset = $dataset->tableData($order_column, $order_by_str, $start, $length)->get();
        $data = [];

        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->can('users edit')) ? 1 : 0;
        $can_delete = ($user->can('users delete')) ? 1 : 0;

        foreach ($dataset as $key => $item) {

            if ($can_edit && $item->id != Auth::id()) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-email='{$item->email}' data-business_id='{$item->institute_id}'  data-mobile='{$item->mobile}'   data-role='{$item->roles[0]->name}' ></i>";
            }
            if ($can_delete) {
                $url = "'users/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            $status_btn =  $item->is_demo ? "<i class='text-danger fa fa-close'/>":"<i class='text-success fa fa-check'/>";

            $data[$i] = array(
                $item->id,
                $item->name,
                $item->email,
                $item->mobile,
                $item->institute_name,
                $item->getRoleNames(),
                $edit_btn . $delete_btn
            );
            $i++;
        }


        $filter_count = $filter_count == 0 ? $channels_count : $filter_count;

        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($filter_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function profile(Request $request, User $user)
    {
        return view('users.profile', compact('user'));
    }


    public function export(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $number = $request->number;
        $biz = $request->institute_id;

        $date_range = $request->registration_date;
        $status = $request->status;
        $type = $request->type;


        $dataset = User::filterData($biz,$email,$name,$number,$date_range,$status)->get();
        $date_string= Carbon::now()->format('YmdHis');
        $file_name = "USERS_$date_string";

        if ($type == "csv") {
            $file_name .= ".csv";
            $writerType = Excel::CSV;
        }else{
            $file_name .= ".xlsx";
            $writerType = Excel::XLSX;
        }

        return (new UsersExport($dataset))->download($file_name, $writerType);
    }
}
