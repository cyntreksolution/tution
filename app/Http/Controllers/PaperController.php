<?php

namespace App\Http\Controllers;

use App\Models\Paper;
use App\Models\PaperClass;
use App\Models\PaperEnroll;
use App\Models\Question;
use App\Models\QuestionEnroll;
use App\Models\Student;
use App\Models\StudentEnroll;
use App\Models\TutionClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = TutionClass::pluck('name', 'id');
        return view('paper.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $paper = new Paper();
        $paper->title = $request->title;
        $paper->duration = $request->duration;
        $paper->marks = $request->marks;
        $paper->is_equal_marks = !empty($request->is_equal_marks) ? 1 : 0;
        $paper->is_shuffle_questions = !empty($request->is_shuffle_questions) ? 1 : 0;
        $paper->is_shuffle_answers = !empty($request->is_shuffle_answers) ? 1 : 0;
        $paper->exam_time = Carbon::parse($request->exam_time)->format('Y-m-d H:i');
        $paper->created_by = Auth::id();
        $paper->save();


        $paper->classes()->sync($request->class_id);
        $msg = 'Paper Created Successfully';
        return $this->sendResponse($paper, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Paper $paper
     * @return \Illuminate\Http\Response
     */
    public function show(Paper $paper)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Paper $paper
     * @return \Illuminate\Http\Response
     */
    public function edit(Paper $paper)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Paper $paper
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paper $paper)
    {
        $paper->title = $request->title;
        $paper->duration = $request->duration;
        $paper->marks = $request->marks;
        $paper->is_equal_marks = !empty($request->is_equal_marks) ? 1 : 0;
        $paper->is_shuffle_questions = !empty($request->is_shuffle_questions) ? 1 : 0;
        $paper->is_shuffle_answers = !empty($request->is_shuffle_answers) ? 1 : 0;
        $paper->exam_time = Carbon::parse($request->exam_time)->format('Y-m-d H:i');
        $paper->save();

        if ($request->status == 'publish') {
            $paper->status = $request->status;
            if ($paper->is_equal_marks) {
                $questions = Question::wherePaperId($paper->id)->get();
                if (!empty($questions) && $questions->count() > 0) {
                    $questions_marks = $paper->marks / $questions->count();
                    foreach ($questions as $question) {
                        $question->marks = $questions_marks;
                        $question->save();
                    }
                    $paper->save();
                } else {
                    return $this->sendError("Paper Marks Does Not Have Any Questions To Publish");
                }

            } else {
                $questions_marks = Question::wherePaperId($paper->id)->sum('marks');
                if ($questions_marks == $paper->marks) {
                    $paper->save();
                } else {
                    return $this->sendError("Paper Marks Not Match to Questions Marks Total");
                }
            }
        } else {
            $paper->status = $request->status;
            $paper->save();
        }


        $paper->classes()->sync($request->class_id);
        $msg = 'Paper Updated Successfully';
        return $this->sendResponse($paper, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Paper $paper
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paper $paper)
    {
        $paper->delete();
        $msg = 'Paper Deleted Successfully';
        return $this->sendResponse([], $msg);
    }

    public function datatable(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'tution_classes.name', 'title', 'duration', 'total_question', 'marks', 'is_equal_marks', 'is_shuffle_questions', 'is_shuffle_answers', 'status', 'exam_time'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Paper::query();
        $filter_count = 0;
        $channels_count = Paper::all()->count();
        if (!empty($request->filter)) {
            $name = $request->name;
            $email = $request->email;
            $number = $request->number;
            $institute = $request->institute_id;
            $date_range = $request->registration_date;
            $status = $request->status;


            $filter_count = Student::filterData($institute, $email, $name, $number, $date_range, $status)
                ->join('paper_class', 'paper.id', '=', 'paper_class.paper_id')
                ->join('tution_classes', 'paper_class.tution_class_id', '=', 'tution_classes.id')
                ->count();

            $dataset = $dataset->filterData($institute, $email, $name, $number, $date_range, $status);


        }

        if (!is_null($search) || !empty($search)) {
            $dataset = $dataset->searchData($search);
            $channels_count = $dataset->count();
        }

        $dataset = $dataset->tableData($order_column, $order_by_str, $start, $length)->get();
        $data = [];

        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $unroll_btn = null;
        $user = Auth::user();
        $can_edit = ($user->can('paper edit')) ? 1 : 0;
        $can_delete = ($user->can('paper delete')) ? 1 : 0;

        foreach ($dataset as $key => $item) {

            if ($can_edit) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-title='{$item->title}' data-email='{$item->email}' data-class_id='{$item->classes->pluck('id')}'  data-duration='{$item->duration}'  data-marks='{$item->marks}'    data-exam_time='{$item->exam_time}'   data-is_equal_marks='{$item->is_equal_marks}'    data-is_shuffle_questions='{$item->is_shuffle_questions}'  data-status='{$item->status}'  data-is_shuffle_answers='{$item->is_shuffle_answers}'   ></i>";
            }
            if ($can_delete) {
                $url = "'papers/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            if ($can_delete) {
                $url = "'papers/remove/" . $item->id . "/" . $item->class_id . " '";
                $unroll_btn = "<i class='fas fa-address-card text-warning mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            $status_btn = $item->status;

            $lbl_is_equal_marks = $item->is_equal_marks ? "<i class='text-primary fas fa-check'/>" : "<i class='text-danger fas fa-times'/>";
            $lbl_is_shuffle_questions = $item->is_shuffle_questions ? "<i class='text-primary fas fa-check'/>" : "<i class='text-danger fas fa-times'/>";
            $lbl_is_shuffle_answers = $item->is_shuffle_answers ? "<i class='text-primary fas fa-check'/>" : "<i class='text-danger fas fa-times'/>";


            $new_question = "<i class='text-primary far fa-newspaper' data-toggle='modal' data-target='#questionModal'></i> ";
            $new_question_upload = "<i class='text-success fas fa-file-import' onclick=\"uploadQuestions(this)\" data-id='{$item->id}' ></i> ";

            $paper = Paper::find($item->id);
            $data[$i] = array(
                $item->id,
                $item->class_name,
                $item->title,
                $item->duration,
                $paper->questions()->count(),
                $item->marks,
                $lbl_is_equal_marks,
                $lbl_is_shuffle_questions,
                $lbl_is_shuffle_answers,
                $status_btn,
                $item->exam_time,
                $edit_btn . $delete_btn . $unroll_btn . $new_question . $new_question_upload
            );
            $i++;
        }


        $filter_count = $filter_count == 0 ? $channels_count : $filter_count;

        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($filter_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function removePaper(Request $request)
    {
        $paper = Paper::find($request->paper_id);
        $class_id = $request->class_id;
        $paper->classes()->detach($class_id);
        $msg = 'Paper Remove Successfully';
        return $this->sendResponse([], $msg);

    }


    public function publishPaper(Request $request,$paper) {
        $pc = PaperClass::find($paper);
        $students = StudentEnroll::whereTutionClassId($pc->tution_class_id)->pluck('student_id');

        if (!empty($students)) {
            $this->enrollPaper($pc->id,$pc->tution_class_id,$pc->paper_id,$students);
        }

        $paper = Paper::find($pc->paper_id);
        $paper->status ='published';
        $paper->save();
    }

    function enrollPaper($paper_clz_id,$clz_id,$paper_id,$students){
        foreach ($students as $student){
            $np = new PaperEnroll();
            $np->student_id = $student;
            $np->paper_class_id = $paper_clz_id;
            $np->tution_class_id = $clz_id;
            $np->paper_id = $paper_id;
            $np->save();
        }
    }


    function enrollQuestions(Request $request,$student_paper_enroll){
        $paper_id = StudentEnroll::whereId($student_paper_enroll)->first()->paper_id;
        $questions = Question::wherePaperId($paper_id)->pluck('id');

        if (!empty($questions)) {
            foreach ($questions as $question){
                $nq = new QuestionEnroll();
                $nq->student_paper_enroll =$student_paper_enroll;
                $nq->paper_id =$paper_id;
                $nq->question_id =$question;
                $nq->save();
            }

        }

    }
}
