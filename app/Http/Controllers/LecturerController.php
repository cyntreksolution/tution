<?php

namespace App\Http\Controllers;

use App\Exports\LecturerExport;
use App\Exports\UsersExport;
use App\Imports\LecturerImport;
use App\Models\Institute;
use App\Models\Lecturer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel as ExcelImport;

class LecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutes = Institute::pluck('name','id');
        return view('lecturer.index',compact('institutes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lecturer =new  User();
        $lecturer->email =rand(9999,99999);
        $lecturer->password = Hash::make('123456789');
        $lecturer->institute_id =$request->institute_id;
        $lecturer->mobile =$request->mobile;
        $lecturer->name =$request->name;
        $lecturer->is_lecturer = 1;
        $lecturer->save();



        $lecturer->assignRole('Lecturer');

        $msg = 'Lecturer Created Successfully';
        return $this->sendResponse($lecturer, $msg);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function show(Lecturer $lecturer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function edit(Lecturer $lecturer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lecturer $lecturer)
    {
        $lecturer->institute_id =$request->institute_id;
        $lecturer->mobile =$request->mobile;
        $lecturer->name =$request->name;
        $lecturer->save();

        $msg = 'Lecturer Created Successfully';
        return $this->sendResponse($lecturer, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lecturer $lecturer)
    {
        $lecturer->delete();
        $msg = 'Lecturer Deleted Successfully';
        return $this->sendResponse([], $msg);
    }


    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id','name', 'email','mobile','institute_name'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Lecturer::query();
        $filter_count =0;
        $channels_count =Lecturer::all()->count();
        if (!empty($request->filter)) {
            $name = $request->name;
            $email = $request->email;
            $number = $request->number;
            $institute = $request->institute_id;
            $date_range = $request->registration_date;
            $status = $request->status;

            $dataset = $dataset->filterData($institute,$email,$name,$number,$date_range,$status);
            $filter_count = $dataset->count();
        }

        if (!is_null($search) || !empty($search)) {
            $dataset = $dataset->searchData($search);
            $channels_count = $dataset->count();
        }

        $dataset = $dataset->tableData($order_column, $order_by_str, $start, $length)->get();
        $data = [];

        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->can('lecturer edit')) ? 1 : 0;
        $can_delete = ($user->can('lecturer delete')) ? 1 : 0;

        foreach ($dataset as $key => $item) {

            if ($can_edit && $item->id != Auth::id()) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-email='{$item->email}' data-institute_id='{$item->institute_id}'  data-mobile='{$item->mobile}'   ></i>";
            }
            if ($can_delete) {
                $url = "'lecturer/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            $status_btn =  $item->is_demo ? "<i class='text-danger fa fa-close'/>":"<i class='text-success fa fa-check'/>";

            $data[$i] = array(
                $item->id,
                $item->name,
                $item->email,
                $item->mobile,
                $item->institute_name,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        $filter_count = $filter_count == 0 ? $channels_count : $filter_count;

        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($filter_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function export(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $number = $request->number;
        $biz = $request->institute_id;

        $date_range = $request->registration_date;
        $status = $request->status;
        $type = $request->type;


        $dataset = Lecturer::filterData($biz,$email,$name,$number,$date_range,$status)->get();
        $date_string= Carbon::now()->format('YmdHis');
        $file_name = "LECTURERS_$date_string";

        if ($type == "csv") {
            $file_name .= ".csv";
            $writerType = Excel::CSV;
        }else{
            $file_name .= ".xlsx";
            $writerType = Excel::XLSX;
        }

        return (new LecturerExport($dataset))->download($file_name, $writerType);
    }


    public function importCSV(Request $request){
        $institute = $request->institute_id;
        ExcelImport::import(new LecturerImport($institute), request()->file('file'));
        return redirect()->route('lecturer.index');
    }
}
