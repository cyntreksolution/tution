<?php

namespace App\Http\Controllers;

use App\Exports\LecturerExport;
use App\Exports\StudentExport;
use App\Imports\LecturerImport;
use App\Imports\StudentImport;
use App\Models\Lecturer;
use App\Models\Student;
use App\Models\StudentEnroll;
use App\Models\TutionClass;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel as ExcelImport;
use PhpParser\Builder\Class_;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = TutionClass::pluck('name', 'id');
        return view('student.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $class_id = $request->class_id;

        $student_old = Student::whereName($request->name)->whereMobile($request->mobile)->first();

        if (!empty($student_old) && $student_old->count() > 0) {
            $flag =StudentEnroll::whereStudentId($student_old->id)->whereTutionClassId($class_id)->exists();
            if (!$flag) {
                $student_old->classes()->attach($class_id);
            }

        }else{
            $student = new  User();
            $student->email = rand(9999, 99999);
            $student->password = Hash::make('123456789');
            $student->mobile = $request->mobile;
            $student->name = $request->name;
            $student->is_student = 1;
            $student->save();

            $student->assignRole('Student');

            $st = Student::find($student->id);
            $st->classes()->attach($class_id);
        }

        $msg = 'Student Created Successfully';
        return $this->sendResponse($student, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class_id = $request->class_id;

        $student = Student::find($id);
        $student->mobile = $request->mobile;
        $student->name = $request->name;
        $student->save();

        $student->classes()->sync($class_id);

        $msg = 'Student Created Successfully';
        return $this->sendResponse($student, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        $msg = 'Student Deleted Successfully';
        return $this->sendResponse([], $msg);
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'name', 'email', 'mobile', 'institute_name'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Student::query();
        $filter_count = 0;
        $channels_count = Student::all()->count();

        if (!empty($request->filter)) {
            $name = $request->name;
            $email = $request->email;
            $number = $request->number;
            $institute = $request->institute_id;
            $date_range = $request->registration_date;
            $status = $request->status;


            $filter_count = Student::filterData($institute, $email, $name, $number, $date_range, $status)
                ->leftJoin('student_enroll', 'student_enroll.student_id', '=', 'users.id')
                ->leftJoin('tution_classes', 'student_enroll.tution_class_id', '=', 'tution_classes.id')
                ->count();

            $dataset = $dataset->filterData($institute, $email, $name, $number, $date_range, $status);


        }

        if (!is_null($search) || !empty($search)) {
            $dataset = $dataset->searchData($search);
            $channels_count = $dataset->count();
        }

        $dataset = $dataset->tableData($order_column, $order_by_str, $start, $length)->get();
        $data = [];

        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $unroll_btn = null;
        $user = Auth::user();
        $can_edit = ($user->can('student edit')) ? 1 : 0;
        $can_delete = ($user->can('student delete')) ? 1 : 0;

        foreach ($dataset as $key => $item) {

            if ($can_edit && $item->id != Auth::id()) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-email='{$item->email}' data-class_id='{$item->class_id}'  data-mobile='{$item->mobile}'   ></i>";
            }
            if ($can_delete) {
                $url = "'students/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            if ($can_delete) {
                $url = "'students/unroll/" . $item->id . " / " . $item->class_id . " '";
                $unroll_btn = "<i class='fas fa-address-card text-warning mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            $status_btn = $item->is_demo ? "<i class='text-danger fa fa-close'/>" : "<i class='text-success fa fa-check'/>";

            $data[$i] = array(
                $item->id,
                $item->name,
                $item->email,
                $item->mobile,
                $item->class_name,
                $edit_btn . $delete_btn . $unroll_btn
            );
            $i++;
        }


        $filter_count = $filter_count == 0 ? $channels_count : $filter_count;

        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($filter_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function export(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $number = $request->number;
        $biz = $request->institute_id;

        $date_range = $request->registration_date;
        $status = $request->status;
        $type = $request->type;


        $dataset = Student::filterData($biz, $email, $name, $number, $date_range, $status)->get();
        $date_string = Carbon::now()->format('YmdHis');
        $file_name = "STUDENTS_$date_string";

        if ($type == "csv") {
            $file_name .= ".csv";
            $writerType = Excel::CSV;
        } else {
            $file_name .= ".xlsx";
            $writerType = Excel::XLSX;
        }

        return (new StudentExport($dataset))->download($file_name, $writerType);
    }


    public function importCSV(Request $request)
    {
        $class_id = $request->class_id;
        ExcelImport::import(new StudentImport($class_id), request()->file('file'));
        return redirect()->route('students.index');
    }

    public function unroll(Request $request)
    {
        $student = Student::find($request->student_id);
        $class_id = $request->class_id;
        $student->classes()->detach($class_id);
        $msg = 'Student Unroll Successfully';
        return $this->sendResponse([], $msg);
    }
}
