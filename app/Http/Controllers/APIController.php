<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\PaperClass;
use App\Models\PaperEnroll;
use App\Models\Question;
use App\Models\QuestionEnroll;
use App\Models\Student;
use App\Models\StudentEnroll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class APIController extends Controller
{
    public function getStudentClasses(Request $request)
    {
        $classes = Student::with('classes', 'classes.institute')->whereId(Auth::id())->first();
        return $this->sendResponse($classes, 'Student Classes List');
    }


    public function getUpcomingClassPapers(Request $request)
    {
        $class_id = $request->tution_class_id;
        $student_id = Auth::id();

        $paper = PaperEnroll::with('paper')
            ->whereStudentId($student_id)
            ->whereTutionClassId($class_id)
            ->get();
        return $paper;
    }

    public function getPaperQuestions(Request $request)
    {
        $questions = [];
        $student_paper_enroll_id = $request->student_paper_enroll_id;
        $exists = QuestionEnroll::where('student_paper_enroll_id', $student_paper_enroll_id)->exists();
        if (!$exists) {
            $this->enrollQuestions($student_paper_enroll_id);
            return $this->getPaperQuestions($request);
        } else {
            $questions = QuestionEnroll::with(['question', 'question.answers'])->where('student_paper_enroll_id', '=', $student_paper_enroll_id)->get();
        }

        return $questions;
    }


    function enrollQuestions($student_paper_enroll)
    {
        $paper_id = PaperEnroll::whereId($student_paper_enroll)->first()->paper_id;
        $questions = Question::wherePaperId($paper_id)->pluck('id');

        if (!empty($questions)) {
            foreach ($questions as $question) {
                $nq = new QuestionEnroll();
                $nq->student_paper_enroll_id = $student_paper_enroll;
                $nq->paper_id = $paper_id;
                $nq->question_id = $question;
                $nq->save();
            }

        }

    }


    public function submitAnswers(Request $request)
    {
        $student_paper_enroll = $request->student_paper_enroll_id;
        $answers = $request->answers;

        if (!empty($answers)) {
            foreach ($answers as $key => $answer) {
                $question = QuestionEnroll::whereStudentPaperEnrollId($student_paper_enroll)->whereQuestionId($key)->first();

                if (!empty($question)) {
                    $question->answer_id = $answer;
                    $question->answered_at = Carbon::now()->format('Y-m-d H:i:s');
                    $question->status = 'answered';
                    $question->save();
                }else{
                    return  $this->sendError("Invalid Question Id Or Invalid Student Paper Enroll Id");
                }

            }

            $this->markPaperQuestions($student_paper_enroll);
            $this->markPaper($student_paper_enroll);


            $paper = PaperEnroll::find($student_paper_enroll);
            $questions = QuestionEnroll::with(['question', 'question.answers'])->where('student_paper_enroll_id', '=', $student_paper_enroll)->get();

            $data['paper'] = $paper;
            $data['questions'] = $questions;
            return $this->sendResponse($data, 'Paper Results');
        }else{
            return  $this->sendError("Missing Required Fields");
        }
    }


    public function markPaperQuestions($student_paper_enroll)
    {
        $questions = QuestionEnroll::whereStudentPaperEnrollId($student_paper_enroll)->get();
        foreach ($questions as $question) {
            $marks = 0;
            $answer = Answer::where('question_id', '=', $question->question_id)->correctAnswer()->first();
            $question->correct_answer = $answer->id;
            $status = $question->answer_id == $answer->id ? 'correct' : 'wrong';
            $question->result = $status;
            $question->status = 'grade';

            if ($status == 'correct') {
                $q = Question::find($question->question_id);
                $marks = $q->marks;
            }

            $question->marks = $marks;
            $question->save();
        }

    }

    public function markPaper($student_paper_enroll)
    {
        $paper = PaperEnroll::find($student_paper_enroll);

        $total_marks = QuestionEnroll::whereStudentPaperEnrollId($student_paper_enroll)->sum('marks');
        $paper->marks = $total_marks;
        $paper->status = 'grade';
        $paper->save();
    }
}
