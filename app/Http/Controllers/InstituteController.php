<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Models\City;
use App\Models\Institute;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InstituteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners = User::pluck('name','id');
        $cities = City::pluck('name_en','id');
        return  view('institute.index',compact('owners','cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $institute  = new Institute();
        $institute->name = $request->name;
        $institute->telephone = $request->telephone;
        $institute->address = $request->address;
        $institute->created_by = Auth::id();
        $institute->owner_id = $request->user;
        $institute->save();

        $user = User::find($request->user);
        $user->institute_id =$institute->id;
        $user->save();

        $institute->cities()->sync($request->city);
        $msg = 'Institute Created Successfully' ;
        return $this->sendResponse($institute, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function show(Institute $institute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function edit(Institute $institute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Institute $institute)
    {
        $institute->name = $request->name;
        $institute->telephone = $request->telephone;
        $institute->address = $request->address;
        $institute->created_by = Auth::id();
        $institute->owner_id = $request->user;
        $institute->save();

        $institute->cities()->sync($request->city);


        $user = User::find($request->user);
        $user->institute_id =$institute->id;
        $user->save();



        $msg = 'Institute Created Successfully' ;
        return $this->sendResponse($institute, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institute $institute)
    {
        $institute->delete();
        return $this->sendResponse('', 'Channel Successfully Deleted');
    }

    public function datatable(Request $request){

        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'institutes.name','users.name','address','telephone','cities.name_en','created_at','status'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Institute::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Institute::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data=[];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
//        $can_edit = ($user->hasPermissionTo('institute edit')) ? 1 : 0;
        $can_edit = 1;
//        $can_delete = ($user->hasPermissionTo('institute delete')) ? 1 : 0;
        $can_delete = 1;

        foreach ($dataset as $key => $item) {
            if ($can_edit) {
                $edit_btn = "<i class='fa fa-pencil-alt text-dark mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-user='{$item->owner_id}' data-address='{$item->address}' data-city='{$item->cities->pluck('id')}' data-telephone='{$item->telephone}'></i>";
            }
            if ($can_delete) {
                $url ="'institute/".$item->id."'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            $btnStatus =$item->status == 'enabled' ? "<button class='btn btn-sm btn-success'>enabled</button>":"<button class='btn btn-sm btn-danger'>disabled</button>";

            $data[$i] = array(
                $item->id,
                $item->name,
                optional($item->owner)->name,
                $item->address,
                $item->cities->pluck('name_en'),
                Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                $btnStatus,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function lecturers(Request $request, $id){
        $lectures = User::whereInstituteId($id)->whereIsStudent(0)->pluck('name','id')->toArray();
        return json_encode($lectures);
    }
}
