<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Institute;
use App\Models\Subject;
use App\Models\TutionClass;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class TutionClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutes = Institute::pluck('name','id');
        $subjects = Subject::pluck('name','id');
        $lecturers = [];

        $user = Auth::user();
        if ($user->hasRole(['Super Admin', 'Admin'])) {
            $lecturers = User::whereIsStudent(0)->pluck('name','id');
        }else{
            $lecturers = User::whereInstituteId($user->institute_id)->whereIsStudent(0)->pluck('name','id');
        }

        return  view('classes.index',compact('institutes','subjects','lecturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tutionClass = TutionClass::create($request->all());
        return  $this->sendResponse($tutionClass,'Class Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TutionClass  $tutionClass
     * @return \Illuminate\Http\Response
     */
    public function show(TutionClass $tutionClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TutionClass  $tutionClass
     * @return \Illuminate\Http\Response
     */
    public function edit(TutionClass $tutionClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TutionClass  $tutionClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $tutionClass = TutionClass::find($id);
        $tutionClass->name = $request->name;
        $tutionClass->institute_id = $request->institute_id;
        $tutionClass->user_id = $request->user_id;
        $tutionClass->grade = $request->grade;
        $tutionClass->academic_year = $request->academic_year;
        $tutionClass->subject_id = $request->subject_id;
        $tutionClass->save();

        return  $this->sendResponse($tutionClass,'Class Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TutionClass  $tutionClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $tutionClass = TutionClass::find($id);
        $tutionClass->delete();
        return  $this->sendResponse([],'Class Deleted Successfully!');
    }


    public function datatable(Request $request){

        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'institutes.name', 'users.name','tution_classes.name','grade','subjects.name','academic_year','status'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = TutionClass::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = TutionClass::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data=[];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
//        $can_edit = ($user->hasPermissionTo('institute edit')) ? 1 : 0;
        $can_edit = 1;
//        $can_delete = ($user->hasPermissionTo('institute delete')) ? 1 : 0;
        $can_delete = 1;

        foreach ($dataset as $key => $item) {
            if ($can_edit) {
                $edit_btn = "<i class='fa fa-pencil-alt text-dark mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-user_id='{$item->user_id}' data-subject_id='{$item->subject_id}'  data-institute_id='{$item->institute_id}' data-grade='{$item->grade}' data-academic_year='{$item->academic_year}'></i>";
            }
            if ($can_delete) {
                $url ="'classes/".$item->id."'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }

            $btnStatus =$item->status == 'enabled' ? "<button class='btn btn-sm btn-success'>enabled</button>":"<button class='btn btn-sm btn-danger'>disabled</button>";

            $data[$i] = array(
                $item->id,
                $item->institute_name,
                $item->lecturer_name,
                $item->name,
                $item->grade,
                $item->subject_name,
                $item->academic_year,
//                $btnStatus,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
