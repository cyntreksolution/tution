<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Institute </label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('institute_id', $institute,null, ['class' => 'form-control','placeholder'=>'Select Institute','autocomplete'=>'off','id'=>'business_id','required']) !!}

        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Mobile Number</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('mobile', null, ['class' => 'form-control','placeholder'=>'Enter Mobile Number','autocomplete'=>'off','id'=>'mobile','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Email</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Enter Email','autocomplete'=>'off','id'=>'email','required']) !!}
        </div>
    </div>
</div>

@if (empty($edit))
    <div class="form-group row">
        <label class="col-form-label text-right col-lg-3 col-sm-12">Password</label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="input-group">
                {!! Form::text('password', null, ['class' => 'form-control','placeholder'=>'Enter Password','autocomplete'=>'off','id'=>'password','required']) !!}
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label text-right col-lg-3 col-sm-12">Confirm Password</label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="input-group">
                {!! Form::text('confirmpassword', null, ['class' => 'form-control','placeholder'=>'Enter Confirm Password','autocomplete'=>'off','id'=>'confirmpassword','required']) !!}
            </div>
        </div>
    </div>
@endif

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Roles</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('roles[]', $roles,null, ['class' => 'form-control','id'=>'roles','required',]) !!}
        </div>
    </div>
</div>
