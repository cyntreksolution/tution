@if (Auth::user()->hasRole(['Admin','Super Admin']) )
    <div class="form-group row">
        <label class="col-form-label text-right col-lg-3 col-sm-12">Institute </label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="input-group">
                {!! Form::select('business_id', $institute,null, ['class' => 'form-control','placeholder'=>'','autocomplete'=>'off','id'=>'business_id_filter']) !!}
            </div>
        </div>
    </div>
@endif

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','id'=>'name_filter']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Email</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('email', null, ['class' => 'form-control','autocomplete'=>'off','id'=>'email_filter']) !!}
        </div>
    </div>
</div>


<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Number</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('number', null, ['class' => 'form-control','autocomplete'=>'off','id'=>'number_filter']) !!}
        </div>
    </div>
</div>




<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Registration Date</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('registration_date', null, ['class' => 'form-control','autocomplete'=>'off','id'=>'registration_date_filter']) !!}
        </div>
    </div>
</div>
