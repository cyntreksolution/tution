@extends('layouts.master')
@section('title','User')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">User List
                            <span
                                class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} 's User list</span>
                        </h3>
                    </div>

                    <div class="card-toolbar">
                        @if (Auth::user()->can('users create'))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#createModal">
                                New User
                            </button>
                        @endif
                    </div>

                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                            <th>Institute</th>
                            <th>Roles</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'users.store', 'method' => 'post','id'=>'createForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('users.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="submitForm('createForm','createModal','datatable')"
                            type="button" class="btn btn-primary">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'users.store', 'method' => 'PATCH','id'=>'editForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('users.form',['edit'=>true])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            onclick="submitForm('editForm','editModal','datatable')">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>


    <div class="modal fade" id="filterModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'users.export', 'method' => 'post','id'=>'filterForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('users.filter-form')
                </div>
                <div class="modal-footer">

                    <button onclick="filterForm('filterForm','filterModal')"
                            type="button" class="btn btn-success"><i class="fa fa-filter mr-1"></i> Filter
                    </button>
                    <button name="type" value="excel" type="submit" class="btn btn-outline-primary"> <i class="fa fa-file-csv"></i> Download Excel</button>
                    <button name="type" value="csv" type="submit" class="btn btn-outline-primary"> <i class="fa fa-file-excel"></i> Download CSV</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <script>
        $('#cities').select2({
            placeholder: "Select Cities",
        });

        $('#registration_date_filter').daterangepicker({
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            maxDate: moment(),
            startTime: moment().startOf('day'),
            endTime: moment().endOf('day'),
            locale: {
                format: 'Y-M-DD H:mm'
            },
            parentEl: "#filterModal",
            ranges: {
                'Today': [moment().startOf('day'), moment()],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        $('#registration_date_filter').val('')

        DataTableOption.initDataTable('datatable', '/users/data/table', true, [0, 'desc']);

        @if (Auth::user()->hasRole(['Admin','Super Admin']) )
        $("#datatable_filter").append("<button class='btn btn-outline-primary ml-1 ' data-toggle='modal' data-target='#filterModal'> <i class='fa fa-filter'/> </button>")
        @endif

        function submitForm(form_id, modal_id, table_id) {
            form_id = '#' + form_id;
            table_id = '#' + table_id;
            let url = $(form_id).attr('action');
            let method = $(form_id).attr('method');

            $(form_id).ajaxSubmit(
                {
                    clearForm: true,
                    url: url,
                    type: method,
                    success: function (result) {
                        ModalOptions.hideModal(modal_id);
                        if (result.success) {
                            let table = $(table_id).DataTable();
                            table.ajax.reload();
                            Notifications.showSuccessMsg(result.message);
                        } else {
                            Notifications.showErrorMsg(result.message);
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        ModalOptions.hideModal(modal_id);
                        Notifications.showErrorMsg(errorThrown);
                    }
                }
            );

        }


        FormValidation.formValidation(
            document.getElementById('createForm'),
            {
                fields: {
                    business_id: {
                        validators: {
                            notEmpty: {
                                message: 'business is required'
                            },
                        }
                    },
                    business_section_id: {
                        validators: {
                            notEmpty: {
                                message: 'business section is required'
                            },
                        }
                    },
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'user Name is required'
                            },
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email is required'
                            },
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Password is required'
                            },
                        }
                    },
                    confirmpassword: {
                        validators: {
                            notEmpty: {
                                message: 'Confirm Password is required'
                            },
                        }
                    },
                    roles: {
                        validators: {
                            notEmpty: {
                                message: 'Roles is required'
                            },
                        }
                    },

                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );


        function edit(item) {
            let id = item.dataset.id;
            let name = item.dataset.name;
            let email = item.dataset.email;
            let role = item.dataset.role;
            let business_id = item.dataset.business_id;
            let business_section_id = item.dataset.business_section_id;

            $("#editForm").find('#name').val(name);
            $("#editForm").find('#email').val(email);
            $("#editForm").find('#password').val(password);

            $("#editForm").find('#roles').val(role).trigger("change");
            $("#editForm").find('#business_id').val(business_id);
            $("#editForm").find('#business_section_id').val(business_section_id).trigger("change");


            $("#editForm").attr('action', '/users-admin/' + id);
            ModalOptions.toggleModal('editModal');
        }

        function filterForm(form_id, modal_id) {
            let modal = '#' + modal_id;
            let business_id = $('#business_id_filter').val();
            let business_section_id = $('#business_section_id_filter').val();
            let name = $('#name_filter').val();
            let email = $('#email_filter').val();
            let number = $('#number_filter').val();
            let registration_date = $('#registration_date_filter').val();
            let status = $('#status_filter').val();

            let table = $('#datatable').DataTable();
            table.ajax.url('/users/data/table?business_id=' + business_id + '&business_section_id=' + business_section_id +'&status=' + status + '&registration_date=' + registration_date + '&email=' + email + '&number=' + number + '&name=' + name + '&filter=' + true).load();
            $(modal).modal('toggle');

        }
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('select[name="business_id"]').on('change', function () {
                var businessID = jQuery(this).val();
                if (businessID) {
                    jQuery.ajax({
                        url: 'dropdownlist/business-section/' + businessID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            jQuery('select[name="business_section_id"]').empty();
                            jQuery.each(data, function (key, value) {
                                $('select[name="business_section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    });
                } else {
                    $('select[name="business_section_id"]').empty();
                }
            });
        });
    </script>
@endpush

@push('css')
    <link href="{{asset('css/daterangepicker.css')}}" rel="stylesheet" type="text/css">

@endpush
