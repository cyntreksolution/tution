<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Institute</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'institute_edit':'institute';@endphp
            {!! Form::select('institute_id', $institutes , null , ['class' => "form-control $clx",'placeholder'=>'Select Institute','autocomplete'=>'off','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Lecturer Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control name','placeholder'=>'Lecturer Name','required','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>


<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Mobile Number</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('mobile', null, ['class' => 'form-control mobile','placeholder'=>'Mobile','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>
