<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Institute Owner</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('user', $owners , null , ['class' => 'form-control user','placeholder'=>'Select User','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Institute Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control name','placeholder'=>'Institute Name','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Cities</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'city_edit':'city';@endphp
            {!! Form::select('city[]', $cities , null , ['class' => "form-control w-100 $clx",'id'=>"$clx",'multiple','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Address</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('address', null, ['class' => 'form-control address','placeholder'=>'Address','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Telephone Number</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::number('telephone', null, ['class' => 'form-control telephone','placeholder'=>'Telephone Number','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>
