@extends('layouts.master')
@section('title','Institutes')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">


            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Institutes List
                            <span
                                class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} 's Institute List</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                            New Institutes
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Institute Name</th>
                            <th>Owner Name</th>
                            <th>Address</th>
                            <th>Cities</th>
                            <th>Registered Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Institute</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'institute.store', 'method' => 'post','id'=>'createForm']) !!}
                <div class="modal-body">
                    @include('institute.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="FormOptions.submitForm('createForm','createModal','datatable')"
                            type="button" class="btn btn-primary">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Institute</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'institute.store', 'method' => 'put','id'=>'editForm']) !!}
                <div class="modal-body">
                    @include('institute.form',['edit' =>true])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button"  onclick="FormOptions.submitForm('editForm','editModal','datatable')" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script>


        $('.city_edit').select2({
            placeholder: "Select Cities",
        })

        $('.city').select2({
            placeholder: "Select Cities",
        })


        DataTableOption.initDataTable('datatable', '/institute/data/table');


        FormValidation.formValidation(
            document.getElementById('createForm'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Institute Name is required'
                            },
                        }
                    },

                    city: {
                        validators: {
                            notEmpty: {
                                message: 'City is required'
                            },
                        }
                    },
                    address: {
                        validators: {
                            notEmpty: {
                                message: 'Address is required'
                            },
                        }
                    },
                    telephone: {
                        validators: {
                            notEmpty: {
                                message: 'Telephone is required'
                            },
                            phone: {
                                country: 'US',
                                message: 'Invalid Telephone'
                            },
                        }
                    },
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );


        function edit(institute) {
            let id = institute.dataset.id;
            let name = institute.dataset.name;
            let address = institute.dataset.address;
            let user = institute.dataset.user;
            let city = institute.dataset.city;
            let telephone = institute.dataset.telephone;
            $("#editForm").find('.name').val(name);
            $("#editForm").find('.address').val(address);

            $("#editForm").find('.telephone').val(telephone);

            // $("#editForm").find('.city_edit').val(city);
            $("#editForm").find('.user').val(user);

            $("#editForm").find('.city_edit').val(JSON.parse(city));
            $("#editForm").find('.city_edit').trigger('change');

            $("#editForm").attr('action', '/institute/' + id);
            ModalOptions.toggleModal('editModal');
        }
    </script>
@endpush
