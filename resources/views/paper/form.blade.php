<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Class</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'class_edit':'class';@endphp
            {!! Form::select('class_id[]', $classes , null , ['class' => "form-control $clx",'autocomplete'=>'off','required','multiple']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Title</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('title', null, ['class' => 'form-control title','placeholder'=>'Title','required','autocomplete'=>'off','required']) !!}
        </div>
    </div>
</div>


<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Duration</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('duration', null, ['class' => 'form-control duration','placeholder'=>'Duration (Minutes)','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Marks</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('marks', null, ['class' => 'form-control marks','placeholder'=>'Marks','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Exam Start At</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'exam_time_edit':'exam_time';@endphp
            {!! Form::text('exam_time', null, ['class' => "form-control $clx",'placeholder'=>'Exam Start At','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Is Equal Marks</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'is_equal_marks_edit':'is_equal_marks';@endphp
            <input name="is_equal_marks" class="{{$clx}}" data-switch="true" type="checkbox"
                   data-size="small"
                   data-on-text="YES" data-off-text="NO" data-on-color="info"
                   data-off-color="success"/>
        </div>
    </div>
</div>


<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Is Shuffle Question</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'is_shuffle_questions_edit':'is_shuffle_questions';@endphp
            <input name="is_shuffle_questions" class="{{$clx}}" data-switch="true" type="checkbox"
                   data-size="small"
                   data-on-text="YES" data-off-text="NO" data-on-color="info"
                   data-off-color="success"/>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Is Shuffle Answers</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'is_shuffle_answers_edit':'is_shuffle_answers';@endphp
            <input name="is_shuffle_answers" class="{{$clx}}" data-switch="true" type="checkbox"
                   data-size="small"
                   data-on-text="YES" data-off-text="NO" data-on-color="info"
                   data-off-color="success"/>
        </div>
    </div>
</div>

@if (!empty($edit))
    <div class="form-group row">
        <label class="col-form-label text-right col-lg-3 col-sm-12">Class</label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="input-group">
                {!! Form::select('status', ['draft'=>'Draft','publish'=>'Publish'] , null , ['class' => "form-control status",'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>

@endif

