@extends('layouts.master')
@section('title','Papers')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">


            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Papers List
                            <span
                                class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} 's Papers List</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                            New Paper
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Class Name</th>
                            <th>Title</th>
                            <th>Duration</th>
                            <th>Total Questions</th>
                            <th>Marks</th>
                            <th>Is Equal Marks</th>
                            <th>Shuffle Questions</th>
                            <th>Shuffle Answers</th>
                            <th>Status</th>
                            <th>Exam Time</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Papers</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'papers.store', 'method' => 'post','id'=>'createForm']) !!}
                <div class="modal-body">
                    @include('paper.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="FormOptions.submitForm('createForm','createModal','datatable')"
                            type="button" class="btn btn-primary">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Papers</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'papers.store', 'method' => 'put','id'=>'editForm']) !!}
                <div class="modal-body">
                    @include('paper.form',['edit' =>true])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" onclick="FormOptions.submitForm('editForm','editModal','datatable')"
                            class="btn btn-primary">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="filterModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'papers.export', 'method' => 'post','id'=>'filterForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('paper.filter-form')
                </div>
                <div class="modal-footer">

                    <button onclick="filterForm('filterForm','filterModal')"
                            type="button" class="btn btn-success"><i class="fa fa-filter mr-1"></i> Filter
                    </button>
                    <button name="type" value="excel" type="submit" class="btn btn-outline-primary"><i
                            class="fa fa-file-csv"></i> Download Excel
                    </button>
                    <button name="type" value="csv" type="submit" class="btn btn-outline-primary"><i
                            class="fa fa-file-excel"></i> Download CSV
                    </button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="questionModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Question</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'questions.store', 'method' => 'post','id'=>'filterForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('paper.question-form')
                </div>
                <div class="modal-footer">
                    <button onclick="FormOptions.submitForm('createForm','createModal','datatable')"
                            type="button" class="btn btn-primary">Save changes
                    </button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="questionImportModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Question</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'questions.store', 'method' => 'post','id'=>'importForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('paper.question-import-form')
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success"><i class="fa fa-filter mr-1"></i> Import</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('css')
    <link href="{{asset('plugins/summernote/summernote.css')}}" rel="stylesheet"/>
@endpush

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <script src="{{asset('plugins/summernote/summernote.js')}}"></script>
    <script>
        $('#summernote').summernote({
            height: 200,
        });
        $('.class_edit').select2({
            placeholder: "Select Class",
        })

        $('.class').select2({
            placeholder: "Select Class",
        })


        $("#editForm").find('.exam_time_edit').daterangepicker({
            "singleDatePicker": true,
            "timePicker": true,
            "startDate": moment(),
            "minDate": moment(),
            locale: {
                format: 'Y-M-DD H:mm'
            },
            parentEl: "#editModal",
        });

        $("#createForm").find('.exam_time').daterangepicker({
            "singleDatePicker": true,
            "timePicker": true,
            "startDate": moment(),
            "minDate": moment(),
            locale: {
                format: 'Y-M-DD H:mm'
            },
            parentEl: "#createModal",
        });

        $("#createForm").find('.exam_time').val('')
        $("#editForm").find('.exam_time').val('')


        function filterForm(form_id, modal_id) {
            let modal = '#' + modal_id;
            let institute_id = $('#institute_id_filter').val();
            let name = $('#name_filter').val();
            let email = $('#email_filter').val();
            let number = $('#number_filter').val();
            let registration_date = $('#registration_date_filter').val();

            let table = $('#datatable').DataTable();
            table.ajax.url('/papers/data/table?institute_id=' + institute_id + '&registration_date=' + registration_date + '&email=' + email + '&number=' + number + '&name=' + name + '&filter=' + true).load();
            $(modal).modal('toggle');

        }


        DataTableOption.initDataTable('datatable', '/papers/data/table');
        $("#datatable_filter").append("<button class='btn btn-outline-primary ml-1 ' data-toggle='modal' data-target='#filterModal'> <i class='fa fa-filter'/> </button>")


        FormValidation.formValidation(
            document.getElementById('createForm'),
            {
                fields: {
                    title: {
                        validators: {
                            notEmpty: {
                                message: 'Papers title is required'
                            },
                        }
                    },
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );

        function uploadQuestions(paper) {
            let id = paper.dataset.id;
            $("#importForm").attr('action', '/papers/' + id + '/questions/import');
            ModalOptions.toggleModal('questionImportModal');
        }

        function edit(paper) {
            let id = paper.dataset.id;
            let title = paper.dataset.title;
            let class_id = paper.dataset.class_id;
            let duration = paper.dataset.duration;
            let status = paper.dataset.status;
            let marks = paper.dataset.marks;
            let is_equal_marks = paper.dataset.is_equal_marks;
            let is_shuffle_questions = paper.dataset.is_shuffle_questions;
            let is_shuffle_answers = paper.dataset.is_shuffle_answers;
            let exam_time = paper.dataset.exam_time;

            $("#editForm").find('.title').val(title);
            $("#editForm").find('.class_edit').val(JSON.parse(class_id)).trigger('change');
            $("#editForm").find('.duration').val(duration);
            $("#editForm").find('.status').val(status);
            $("#editForm").find('.marks').val(marks);
            $("#editForm").find('.is_equal_marks').val(is_equal_marks);
            $("#editForm").find('.is_shuffle_questions').val(is_shuffle_questions);
            $("#editForm").find('.is_shuffle_answers').val(is_shuffle_answers);
            $("#editForm").find('.exam_time_edit').val(exam_time);


            $("#editForm").attr('action', '/papers/' + id);
            ModalOptions.toggleModal('editModal');
        }

        // $('#schedule-switch').on('switchChange.bootstrapSwitch', function (event, state) {
        //     if (state) {
        //         $('#schedule_zone').removeClass('d-none');
        //         $('#schedule_time').attr('required');
        //     } else {
        //         $('#schedule_zone').addClass('d-none');
        //         $('#schedule_time').removeAttr('required');
        //     }
        // });

        $('[data-switch=true]').bootstrapSwitch();
    </script>
@endpush
