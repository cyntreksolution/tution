<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Class</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'class_edit':'class';@endphp
            {!! Form::select('class_id', $classes , null , ['class' => "form-control $clx",'placeholder'=>'Select Class','autocomplete'=>'off','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Student Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control name','placeholder'=>'Lecturer Name','required','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>


<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Mobile Number</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('mobile', null, ['class' => 'form-control mobile','placeholder'=>'Mobile','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>
