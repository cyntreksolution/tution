<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Class </label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('class_id', $classes,null, ['class' => 'form-control','placeholder'=>'','autocomplete'=>'off','id'=>'institute_id_filter','required']) !!}
        </div>
    </div>
</div>



<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">File</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::file('file', ['class' => 'form-control','required']) !!}
        </div>
        <div class="mt-3">
            <i class="fa fa-download"></i> <a href="example_files/student-csv.csv" class="mt-5" download>Download example file</a>
        </div>
    </div>


</div>

