@extends('layouts.master')
@section('title','Students')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">


            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Students List
                            <span
                                class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} 's Students List</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <button type="button" class="btn btn-success mr-2" data-toggle="modal" data-target="#importModal">
                            Import Students
                        </button>

                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                            New Student
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>User ID</th>
                            <th>Contact Number</th>
                            <th>Class Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Students</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'students.store', 'method' => 'post','id'=>'createForm']) !!}
                <div class="modal-body">
                    @include('student.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="FormOptions.submitForm('createForm','createModal','datatable')"
                            type="button" class="btn btn-primary">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Students</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'students.store', 'method' => 'put','id'=>'editForm']) !!}
                <div class="modal-body">
                    @include('student.form',['edit' =>true])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" onclick="FormOptions.submitForm('editForm','editModal','datatable')"
                            class="btn btn-primary">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="filterModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'students.export', 'method' => 'post','id'=>'filterForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('student.filter-form')
                </div>
                <div class="modal-footer">

                    <button onclick="filterForm('filterForm','filterModal')"
                            type="button" class="btn btn-success"><i class="fa fa-filter mr-1"></i> Filter
                    </button>
                    <button name="type" value="excel" type="submit" class="btn btn-outline-primary"><i
                            class="fa fa-file-csv"></i> Download Excel
                    </button>
                    <button name="type" value="csv" type="submit" class="btn btn-outline-primary"><i
                            class="fa fa-file-excel"></i> Download CSV
                    </button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="importModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'students.import', 'method' => 'post','id'=>'filterForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('student.import-form')
                </div>
                <div class="modal-footer">

                    <button class="btn btn-success"><i class="fa fa-filter mr-1"></i> Import</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <script>

        $('.class_edit').select2({
            placeholder: "Select Class",
        })

        $('.class').select2({
            placeholder: "Select Class",
        })

        $('#registration_date_filter').daterangepicker({
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            maxDate: moment(),
            startTime: moment().startOf('day'),
            endTime: moment().endOf('day'),
            locale: {
                format: 'Y-M-DD H:mm'
            },
            parentEl: "#filterModal",
            ranges: {
                'Today': [moment().startOf('day'), moment()],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        $('#registration_date_filter').val('')


        function filterForm(form_id, modal_id) {
            let modal = '#' + modal_id;
            let institute_id = $('#institute_id_filter').val();
            let name = $('#name_filter').val();
            let email = $('#email_filter').val();
            let number = $('#number_filter').val();
            let registration_date = $('#registration_date_filter').val();

            let table = $('#datatable').DataTable();
            table.ajax.url('/students/data/table?institute_id=' + institute_id + '&registration_date=' + registration_date + '&email=' + email + '&number=' + number + '&name=' + name + '&filter=' + true).load();
            $(modal).modal('toggle');

        }


        DataTableOption.initDataTable('datatable', '/students/data/table');
        $("#datatable_filter").append("<button class='btn btn-outline-primary ml-1 ' data-toggle='modal' data-target='#filterModal'> <i class='fa fa-filter'/> </button>")


        FormValidation.formValidation(
            document.getElementById('createForm'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Students Name is required'
                            },
                        }
                    },

                    institute: {
                        validators: {
                            notEmpty: {
                                message: 'Institute is required'
                            },
                        }
                    }
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );


        function edit(Students) {
            let id = Students.dataset.id;
            let name = Students.dataset.name;
            let mobile = Students.dataset.mobile;
            let class_id = Students.dataset.class_id;

            $("#editForm").find('.name').val(name);
            $("#editForm").find('.mobile').val(mobile);
            $("#editForm").find('.class_edit').val(class_id).trigger('change');


            $("#editForm").attr('action', '/students/' + id);
            ModalOptions.toggleModal('editModal');
        }
    </script>
@endpush
