<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8"/>
    <title>@yield('title', 'Dashboard') | {{env('APP_NAME')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="SMS Gateway"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-193649959-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-193649959-1');
    </script>
    @include('meta')
    {{--    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>--}}
    {{--    <link href="{{asset('plugins/custom/fullcalendar/fullcalendar.bundle1ff3.css?v=7.1.2')}}" rel="stylesheet"--}}
    {{--          type="text/css"/>--}}
    <link href="{{asset('plugins/global/plugins.bundle1ff3.css?v=7.1.2')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/custom/prismjs/prismjs.bundle1ff3.css?v=7.1.2')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/style.bundle1ff3.css?v=7.1.2')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/custom/datatables/datatables.bundle1ff3.css?v=7.1.2')}}" rel="stylesheet"/>
    <link href="{{asset('plugins/jquery-toast-plugin/jquery.toast.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <style>

        .select2-container {
            width: 100% !important;
        }

        .loading {

        }


        .loading-inner {
            position: fixed;
            z-index: 9999999;
            overflow: visible;
            margin: auto;
            height: 50px;
            width: 50px;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        .loading:before {
            z-index: 999999;
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.3);
        }
    </style>
    @stack('css')
</head>

<body id="kt_body" style="background-image: url({{asset('media/bg/bg-10.jpg')}})"
      class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">
<div id="kt_header_mobile" class="header-mobile">
    <!--begin::Logo-->
    <a href="/">
        {{--        <img alt="Logo" src=""--}}
        {{--             class="logo-default max-h-30px"/>--}}
    </a>
    <!--end::Logo-->
    <!--begin::Toolbar-->
    <div class="d-flex align-items-center">
        <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
            <span></span>
        </button>
        <button class="btn btn-icon btn-hover-transparent-white p-0 ml-3" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/User.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24"/>
								<path
                                    d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                    fill="#000000" fill-rule="nonzero" opacity="0.3"/>
								<path
                                    d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                    fill="#000000" fill-rule="nonzero"/>
							</g>
						</svg>
                        <!--end::Svg Icon-->
					</span>
        </button>
    </div>
    <!--end::Toolbar-->
</div>
<!--end::Header Mobile-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header-->
            <div id="kt_header" class="header header-fixed">
                <!--begin::Container-->
                <div class="container d-flex align-items-stretch justify-content-between">
                    <!--begin::Left-->
                    <div class="d-flex align-items-stretch mr-3">
                        <!--begin::Header Logo-->
                        <div class="header-logo">
                            <a href="/">
                                {{--                                <p>C</p>--}}
                                {{--                                                                                                <img alt="Logo" src="{{asset('media/misc/KentacyWhite.png')}}"--}}
                                {{--                                                                                                     class="logo-default max-h-40px"/>--}}
                                {{--                                <img alt="Logo" src="../theme/html/demo2/dist/assets/media/logos/logo-letter-1.png"--}}
                                {{--                                     class="logo-sticky max-h-40px"/>--}}
                            </a>
                        </div>

                        <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                            <div id="kt_header_menu"
                                 class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
                                @include('includes.menu')
                            </div>
                        </div>

                    </div>

                    <div class="topbar">
                        <!--begin::Search-->
                        <div class="dropdown">

                            <div
                                class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                                <div class="quick-search quick-search-dropdown" id="kt_quick_search_dropdown">
                                    <!--begin:Form-->
                                    <form method="get" class="quick-search-form">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<span class="svg-icon svg-icon-lg">
																<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/Search.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"/>
																		<path
                                                                            d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                                            fill="#000000" fill-rule="nonzero"
                                                                            opacity="0.3"/>
																		<path
                                                                            d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                                            fill="#000000" fill-rule="nonzero"/>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
														</span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Search..."/>
                                            <div class="input-group-append">
														<span class="input-group-text">
															<i class="quick-search-close ki ki-close icon-sm text-muted"></i>
														</span>
                                            </div>
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                    <!--begin::Scroll-->
                                    <div class="quick-search-wrapper scroll" data-scroll="true" data-height="325"
                                         data-mobile-height="200"></div>
                                    <!--end::Scroll-->
                                </div>
                            </div>
                            <!--end::Dropdown-->
                        </div>
                        <!--end::Search-->
                        <!--begin::Notifications-->
                        <div class="dropdown">
                            <!--begin::Toggle-->
                            <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                                <div
                                    class="btn btn-icon btn-hover-transparent-white btn-dropdown btn-lg mr-1 pulse pulse-primary">
											<span class="svg-icon svg-icon-xl">
												<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Code/Compiling.svg-->
												<svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24"/>
														<path
                                                            d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z"
                                                            fill="#000000" opacity="0.3"/>
														<path
                                                            d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z"
                                                            fill="#000000"/>
													</g>
												</svg>
											</span>
                                    <span class="pulse-ring"></span>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown">
                            <!--begin::Toggle-->
                            <div class="topbar-item">
                                <div
                                    class="btn btn-icon btn-hover-transparent-white d-flex align-items-center btn-lg px-md-2 w-md-auto"
                                    id="kt_quick_user_toggle">
                                    <span
                                        class="text-white opacity-70 font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                                    <span
                                        class="text-white opacity-90 font-weight-bolder font-size-base d-none d-md-inline mr-4">{{Auth::user()->name}}</span>
                                </div>
                            </div>
                            <!--end::Toggle-->
                        </div>
                        <!--end::User-->
                    </div>
                    <!--end::Topbar-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                {{--                <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">--}}
                {{--                    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">--}}
                {{--                        <!--begin::Info-->--}}
                {{--                        <div class="d-flex align-items-center flex-wrap mr-1">--}}
                {{--                            <!--begin::Heading-->--}}
                {{--                            <div class="d-flex flex-column">--}}

                {{--                                <h2 class="text-white font-weight-bold my-2 mr-5">@yield('title', 'Dashboard')</h2>--}}

                {{--                                <div class="d-flex align-items-center font-weight-bold my-2">--}}

                {{--                                    <a href="/dashboard" class="opacity-75 hover-opacity-100">--}}
                {{--                                        <i class="flaticon2-shelter text-white icon-1x"></i>--}}
                {{--                                    </a>--}}

                {{--                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>--}}
                {{--                                    <a href="#"--}}
                {{--                                       class="text-white text-hover-white opacity-75 hover-opacity-100">@yield('title', 'Dashboard')</a>--}}

                {{--                                </div>--}}
                {{--                                <!--end::Breadcrumb-->--}}
                {{--                            </div>--}}
                {{--                            <!--end::Heading-->--}}
                {{--                        </div>--}}

                {{--                    </div>--}}
                {{--                </div>--}}


                @yield('content')
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted font-weight-bold mr-2">{{date('Y')}}©</span>
                        <a href="/" target="_blank" class="text-dark-75 text-hover-primary">Cyntrek Solutions</a>
                    </div>
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>


<!-- begin::User Panel-->
<div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
    <!--begin::Header-->
    <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
        <h3 class="font-weight-bold m-0">User Profile </h3>
        <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
            <i class="ki ki-close icon-xs text-muted"></i>
        </a>
    </div>
    <!--end::Header-->
    <!--begin::Content-->
    <div class="offcanvas-content pr-5 mr-n5">
        <!--begin::Header-->
        <div class="d-flex align-items-center mt-5">
            <div class="symbol symbol-100 mr-5">
                @php
                    $profile_pic = !empty(Auth::user()->profile_photo_path)?Auth::user()->profile_photo_path:'media/misc/boy.svg' ;
                @endphp
                <div class="symbol-label" style="background-image:url({{asset($profile_pic)}})"></div>
                <i class="symbol-badge bg-success"></i>
            </div>
            <div class="d-flex flex-column">
                <a href="#"
                   class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>
                <div class="text-muted mt-1">{{Auth::user()->getRoleNames()[0]}}</div>
                <div class="navi mt-2">
                    <a href="#" class="navi-item">
								<span class="navi-link p-0 pb-2">
									<span class="navi-icon mr-1">
										<span class="svg-icon svg-icon-lg svg-icon-primary">
											<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Mail-notification.svg-->
											<svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                 viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path
                                                        d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z"
                                                        fill="#000000"/>
													<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"/>
												</g>
											</svg>
                                            <!--end::Svg Icon-->
										</span>
									</span>
									<span
                                        class="navi-text text-muted text-hover-primary">{{\Illuminate\Support\Facades\Auth::user()->email}}</span>
								</span>
                    </a>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"
                       class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">Sign Out</a>
                    <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Separator-->
        <div class="separator separator-dashed mt-8 mb-5"></div>
        <!--end::Separator-->
        <!--begin::Nav-->
        <div class="navi navi-spacer-x-0 p-0">
            <!--begin::Item-->
            <a href="{{route('user.profile',Auth::id())}}" class="navi-item">
                <div class="navi-link">
                    <div class="symbol symbol-40 bg-light mr-3">
                        <div class="symbol-label">
									<span class="svg-icon svg-icon-md svg-icon-success">
										<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/General/Notification2.svg-->
										<svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24"/>
												<path
                                                    d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                                                    fill="#000000"/>
												<circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5"/>
											</g>
										</svg>
                                        <!--end::Svg Icon-->
									</span>
                        </div>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">My Profile</div>
                        <div class="text-muted">Account settings and more
                            <span class="label label-light-danger label-inline font-weight-bold">update</span></div>
                    </div>
                </div>
            </a>
            <!--end:Item-->
            <!--begin::Item-->
{{--            <a href="{{route('business.show',[Auth::user()->business->id])}}" class="navi-item">--}}
{{--                <div class="navi-link">--}}
{{--                    <div class="symbol symbol-40 bg-light mr-3">--}}
{{--                        <div class="symbol-label">--}}
{{--									<span class="svg-icon svg-icon-md svg-icon-warning">--}}
{{--										<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Shopping/Chart-bar1.svg-->--}}
{{--										<svg xmlns="http://www.w3.org/2000/svg"--}}
{{--                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"--}}
{{--                                             viewBox="0 0 24 24" version="1.1">--}}
{{--											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--												<rect x="0" y="0" width="24" height="24"/>--}}
{{--												<rect fill="#000000" opacity="0.3" x="12" y="4" width="3" height="13"--}}
{{--                                                      rx="1.5"/>--}}
{{--												<rect fill="#000000" opacity="0.3" x="7" y="9" width="3" height="8"--}}
{{--                                                      rx="1.5"/>--}}
{{--												<path--}}
{{--                                                    d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z"--}}
{{--                                                    fill="#000000" fill-rule="nonzero"/>--}}
{{--												<rect fill="#000000" opacity="0.3" x="17" y="11" width="3" height="6"--}}
{{--                                                      rx="1.5"/>--}}
{{--											</g>--}}
{{--										</svg>--}}
{{--                                        <!--end::Svg Icon-->--}}
{{--									</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="navi-text">--}}
{{--                        <div class="font-weight-bold">My Business</div>--}}
{{--                        <div class="text-muted">Enable and Disable Services</div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </a>--}}
            {{--            <!--end:Item-->--}}
            {{--            <!--begin::Item-->--}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="symbol symbol-40 bg-light mr-3">
                        <div class="symbol-label">
									<span class="svg-icon svg-icon-md svg-icon-danger">
										<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Files/Selected-file.svg-->
										<svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24"/>
												<path
                                                    d="M4.85714286,1 L11.7364114,1 C12.0910962,1 12.4343066,1.12568431 12.7051108,1.35473959 L17.4686994,5.3839416 C17.8056532,5.66894833 18,6.08787823 18,6.52920201 L18,19.0833333 C18,20.8738751 17.9795521,21 16.1428571,21 L4.85714286,21 C3.02044787,21 3,20.8738751 3,19.0833333 L3,2.91666667 C3,1.12612489 3.02044787,1 4.85714286,1 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z"
                                                    fill="#000000" fill-rule="nonzero" opacity="0.3"/>
												<path
                                                    d="M6.85714286,3 L14.7364114,3 C15.0910962,3 15.4343066,3.12568431 15.7051108,3.35473959 L20.4686994,7.3839416 C20.8056532,7.66894833 21,8.08787823 21,8.52920201 L21,21.0833333 C21,22.8738751 20.9795521,23 19.1428571,23 L6.85714286,23 C5.02044787,23 5,22.8738751 5,21.0833333 L5,4.91666667 C5,3.12612489 5.02044787,3 6.85714286,3 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z"
                                                    fill="#000000" fill-rule="nonzero"/>
											</g>
										</svg>
                                        <!--end::Svg Icon-->
									</span>
                        </div>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">Documentation</div>
                        <div class="text-muted">How to use Web / API <span
                                class="label label-light-info label-inline font-weight-bold">coming soon</span></div>
                    </div>
                </div>
            </a>
        {{--            <!--end:Item-->--}}
        {{--            <!--begin::Item-->--}}
        {{--            <a href="#" class="navi-item">--}}
        {{--                <div class="navi-link">--}}
        {{--                    <div class="symbol symbol-40 bg-light mr-3">--}}
        {{--                        <div class="symbol-label">--}}
        {{--									<span class="svg-icon svg-icon-md svg-icon-primary">--}}
        {{--										<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Mail-opened.svg-->--}}
        {{--										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
        {{--											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
        {{--												<rect x="0" y="0" width="24" height="24" />--}}
        {{--												<path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3" />--}}
        {{--												<path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000" />--}}
        {{--											</g>--}}
        {{--										</svg>--}}
        {{--                                        <!--end::Svg Icon-->--}}
        {{--									</span>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                    <div class="navi-text">--}}
        {{--                        <div class="font-weight-bold">My Tasks</div>--}}
        {{--                        <div class="text-muted">latest tasks and projects</div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </a>--}}
        <!--end:Item-->
        </div>
        <!--end::Nav-->
    </div>
    <!--end::Content-->
</div>
<!-- end::User Panel-->

<div id="ajx_load" class="d-none">
    <div class="loading" style="background-color: red">
        {{--        <img class="loading-inner" src="{{asset('media/misc/infinity.gif')}}">--}}
    </div>
</div>


<script src="{{asset('plugins/global/plugins.bundle1ff3.js?v=7.1.2')}}"></script>
<script src="{{asset('plugins/custom/prismjs/prismjs.bundle1ff3.js?v=7.1.2')}}"></script>
<script src="{{asset('js/scripts.bundle1ff3.js?v=7.1.2')}}"></script>
{{--<script src="{{asset('plugins/custom/fullcalendar/fullcalendar.bundle1ff3.js?v=7.1.2')}}"></script>--}}
<script src="{{asset('js/pages/widgets1ff3.js?v=7.1.2')}}"></script>
<script src="{{asset('plugins/custom/datatables/datatables.bundle1ff3.js?v=7.1.2')}}"></script>
<script src="{{asset('js/pages/crud/forms/widgets/select21ff3.js')}}"></script>
<script src="https://kit.fontawesome.com/18602b848e.js" crossorigin="anonymous"></script>
<script>var KTAppSettings = {
        "breakpoints": {"sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200},
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#6993FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#F3F6F9",
                    "dark": "#212121"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1E9FF",
                    "secondary": "#ECF0F3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#212121",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#ECF0F3",
                "gray-300": "#E5EAEE",
                "gray-400": "#D6D6E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#80808F",
                "gray-700": "#464E5F",
                "gray-800": "#1B283F",
                "gray-900": "#212121"
            }
        },
        "font-family": "Poppins"
    };</script>

<script src="{{asset('js/jquery.form.js')}}"></script>
<script src="{{asset('js/notifications.js')}}"></script>
<script src="{{asset('js/modal.js')}}"></script>
<script src="{{asset('js/dataTable.js')}}"></script>

<script src="{{asset('js/FormOptions.js')}}"></script>
<script src="{{asset('plugins/jquery-toast-plugin/jquery.toast.min.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/additional-methods.min.js')}}"></script>
<script src="{{asset('js/select2.min.js')}}"></script>
<script src="{{asset('js/select2.full.min.js')}}"></script>


<script>

    $(document).ajaxSend(function () {
        $('#ajx_load').removeClass('d-none')
    });
    $(document).ajaxComplete(function () {
        $('#ajx_load').addClass('d-none')
    });
    @if(Session::has('message'))
    let type = "{{ Session::get('alert-type', 'info') }}";
    let msg = "{{ Session::get('message') }}";
    switch (type) {
        case 'info':
            Notifications.showSuccessMsg(msg);
            break;
        case 'warning':
            Notifications.showSuccessMsg(msg);
            break;
        case 'success':
            Notifications.showSuccessMsg(msg);
            break;
        case 'error':
            Notifications.showErrorMsg(msg);
            break;
    }
    @endif
</script>
@stack('js')
</body>

</html>

