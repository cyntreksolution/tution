@extends('layouts.master')
@section('title','Classes')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">


            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Classes List
                            <span
                                class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} 's Class List</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                            New Class
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Institute Name</th>
                            <th>Lecturer Name</th>
                            <th>Class Name</th>
                            <th>Grade</th>
                            <th>Subject</th>
                            <th>Academic Year</th>
{{--                            <th>Status</th>--}}
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Class</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'classes.store', 'method' => 'post','id'=>'createForm']) !!}
                <div class="modal-body">
                    @include('classes.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="FormOptions.submitForm('createForm','createModal','datatable')"
                            type="button" class="btn btn-primary">Save changes
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Class</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'classes.store', 'method' => 'put','id'=>'editForm']) !!}
                <div class="modal-body">
                    @include('classes.form',['edit' =>true])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button"  onclick="FormOptions.submitForm('editForm','editModal','datatable')" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script>

        $('.institute_edit').select2({
            placeholder: "Select Institute",
        })

        $('.institute').select2({
            placeholder: "Select Institute",
        })

        $('.subject_edit').select2({
            placeholder: "Select Subject",
        })

        $('.subject').select2({
            placeholder: "Select Subject",
        })

        $('.lecturer').select2({
            placeholder: "Select Lecturer",
        })

        $('.lecturer_edit').select2({
            placeholder: "Select Lecturer",
        })







        DataTableOption.initDataTable('datatable', '/classes/data/table');


        FormValidation.formValidation(
            document.getElementById('createForm'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Class Name is required'
                            },
                        }
                    },

                    institute: {
                        validators: {
                            notEmpty: {
                                message: 'Institute is required'
                            },
                        }
                    }
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );


        function edit(tution_clas) {
            let id = tution_clas.dataset.id;
            let institute = tution_clas.dataset.institute_id;
            let user_id = tution_clas.dataset.user_id;
            let name = tution_clas.dataset.name;
            let grade = tution_clas.dataset.grade;
            let subject = tution_clas.dataset.subject_id;
            let academic_year = tution_clas.dataset.academic_year;
            let status = tution_clas.dataset.status;

            $("#editForm").find('.name').val(name);
            $("#editForm").find('.institute_edit').val(institute);
            $("#editForm").find('.institute_edit').trigger('change');

            $("#editForm").find('.grade').val(grade);

            $("#editForm").find('.subject_edit').val(subject);
            $("#editForm").find('.subject_edit').trigger('change');

            $("#editForm").find('.lecturer_edit').val(user_id);
            $("#editForm").find('.lecturer_edit').trigger('change');

            $("#editForm").find('.academic_year').val(academic_year);
            $("#editForm").find('.status').val(status);


            $("#editForm").attr('action', '/classes/' + id);
            ModalOptions.toggleModal('editModal');
        }
    </script>
@endpush
