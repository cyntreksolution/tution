<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Institute</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'institute_edit':'institute';@endphp
            {!! Form::select('institute_id', $institutes , null , ['class' => "form-control $clx",'placeholder'=>'Select Institute','autocomplete'=>'off','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Lecturer</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'lecturer_edit':'lecturer';@endphp
            {!! Form::select('user_id', $lecturers , null , ['class' => "form-control $clx",'placeholder'=>'Select Lecturer','autocomplete'=>'off','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Class Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control name','placeholder'=>'Class Name','required','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>


<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Grade</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('grade', null, ['class' => 'form-control grade','placeholder'=>'Grade','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Academic Year</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::number('academic_year', null, ['class' => 'form-control academic_year','min'=>2021,'max'=>2099,'placeholder'=>'Academic Year','autocomplete'=>'off']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Subject</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            @php $clx =!empty($edit) && $edit ? 'subject_edit':'subject';@endphp
            {!! Form::select('subject_id', $subjects , null , ['class' => "form-control $clx",'placeholder'=>'Select Subject','autocomplete'=>'off','required']) !!}
        </div>
    </div>
</div>

