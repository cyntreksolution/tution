<?php

use App\Http\Controllers\DebugController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InstituteController;
use App\Http\Controllers\LecturerController;
use App\Http\Controllers\PaperController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PermissionGroupController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TutionClassController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/debug', [DebugController::class, 'index'])->name('debug');

Route::resource('roles', RoleController::class)->middleware('role_or_permission:Super Admin');
Route::get('roles/data/table', [RoleController::class, 'datatable'])->middleware('role_or_permission:Super Admin');

Route::resource('permission-groups', PermissionGroupController::class)->middleware('role_or_permission:Super Admin');
Route::get('permission-groups/data/table', [PermissionGroupController::class, 'datatable'])->middleware('role_or_permission:Super Admin');

Route::resource('permissions', PermissionController::class)->middleware('role_or_permission:Super Admin');
Route::get('permissions/data/table', [PermissionController::class, 'datatable'])->middleware('role_or_permission:Super Admin');

Route::resource('users', UserController::class);
Route::get('users/data/table', [UserController::class, 'datatable']);
Route::post('users/data/export', [UserController::class, 'export'])->name('users.export');
Route::patch('users-admin/{user}', [UserController::class, 'updateByAdmin']);
Route::get('users/profile/{user}', [UserController::class, 'profile'])->name('user.profile');
Route::patch('users/profile/{user}/update-password', [UserController::class, 'updatePassword'])->name('update.password');
Route::get('dropdownlist/business-section/{id}',[UserController::class, 'getBusinessSection']);


Route::resource('institute', InstituteController::class);
Route::get('institute/data/table', [InstituteController::class, 'datatable'])->name('datatable');
Route::get('institute/{id}/lecturer', [InstituteController::class, 'lecturers'])->name('institute.lecturer');

Route::resource('classes', TutionClassController::class);
Route::get('classes/data/table', [TutionClassController::class, 'datatable'])->name('datatable');

Route::resource('lecturer', LecturerController::class);
Route::get('lecturer/data/table', [LecturerController::class, 'datatable'])->name('datatable');
Route::post('lecturer/data/export', [LecturerController::class, 'export'])->name('lecturer.export');
Route::post('lecturer/data/import', [LecturerController::class, 'importCSV'])->name('lecturer.import');

Route::resource('students', StudentController::class);
Route::get('students/data/table', [StudentController::class, 'datatable'])->name('datatable');
Route::post('students/data/export', [StudentController::class, 'export'])->name('students.export');
Route::post('students/data/import', [StudentController::class, 'importCSV'])->name('students.import');
Route::delete('students/unroll/{student_id}/{class_id}', [StudentController::class, 'unroll'])->name('students.unroll');


Route::resource('papers', PaperController::class);
Route::get('papers/data/table', [PaperController::class, 'datatable'])->name('papers.datatable');
Route::post('papers/data/export', [PaperController::class, 'export'])->name('papers.export');
Route::delete('papers/remove/{paper_id}/{class_id}', [PaperController::class, 'removePaper'])->name('papers.remove');
Route::get('papers/{paper}/publish', [PaperController::class, 'publishPaper'])->name('papers.publish');


Route::resource('questions', QuestionController::class);
Route::get('questions/data/table', [QuestionController::class, 'datatable'])->name('questions.datatable');
Route::post('papers/{paper_id}/questions/import', [QuestionController::class, 'importCSV'])->name('questions.import');
