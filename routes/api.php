<?php

use App\Http\Controllers\APIController;
use App\Http\Controllers\DebugController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {
    Route::get('/get/classes', [APIController::class,'getStudentClasses']);
    Route::post('/get/class/upcoming-papers', [APIController::class,'getUpcomingClassPapers']);
    Route::post('/get/paper/questions', [APIController::class,'getPaperQuestions']);
    Route::post('/submit/paper/answers', [APIController::class,'submitAnswers']);
});
