gsm7bitChars = "@£$¥èéùìòÇ\\nØø\\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\\\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà";
gsm7bitExChar = "\\^{}\\\\\\[~\\]|€";
gsm7bitRegExp = RegExp("^[" + SmsCounter.gsm7bitChars + "]*$");
gsm7bitExRegExp = RegExp("^[" + SmsCounter.gsm7bitChars + SmsCounter.gsm7bitExChar + "]*$");
gsm7bitExOnlyRegExp = RegExp("^[\\" + SmsCounter.gsm7bitExChar + "]*$");
GSM_7BIT = "GSM_7BIT";
GSM_7BIT_EX = "GSM_7BIT_EX";
UTF16 = "UTF16";
messageLength = {GSM_7BIT: 160, GSM_7BIT_EX: 160, UTF16: 70};
multiMessageLength = {GSM_7BIT: 153, GSM_7BIT_EX: 153, UTF16: 67};


let emo = $("#message").emojioneArea({
    events: {
        keyup: function (editor, event) {
            countChar(this);
        },
        change: function (editor, event) {
            countChar(this);
        },
        emojibtn_click: function (editor, event) {
            countChar(this);
        }
    }
});


function countChar(val) {
    text = val.getText();
    var count, encoding, length, messages, per_message, remaining;
    encoding = this.detectEncoding(text);
    length = text.length;
    if (encoding === this.GSM_7BIT_EX) {
        length += this.countGsm7bitEx(text)
    }
    per_message = this.messageLength[encoding];
    if (length > per_message) {
        per_message = this.multiMessageLength[encoding]
    }
    messages = Math.ceil(length / per_message);
    remaining = per_message * messages - length;
    if (remaining == 0 && messages == 0) {
        remaining = per_message;
    }
    count = {
        encoding: encoding,
        length: length,
        per_message: per_message,
        remaining: remaining,
        messages: messages
    }
    target = $('#sms-counter');
    _results = [];
    for (k in count) {
        v = count[k];
        _results.push(target.find("." + k).text(v))
    }
    return _results
}

detectEncoding = function (text) {
    switch (false) {
        case text.match(this.gsm7bitRegExp) == null:
            return this.GSM_7BIT;
        case text.match(this.gsm7bitExRegExp) == null:
            return this.GSM_7BIT_EX;
        default:
            return this.UTF16
    }
}

countGsm7bitEx = function (text) {
    var char2, chars;
    chars = function () {
        var _i, _len, _results;
        _results = [];
        for (_i = 0, _len = text.length; _i < _len; _i++) {
            char2 = text[_i];
            if (char2.match(this.gsm7bitExOnlyRegExp) != null) {
                _results.push(char2)
            }
        }
        return _results
    }.call(this);
    return chars.length
}


$('#schedule-switch').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
        $('#schedule_zone').removeClass('d-none');
        $('#schedule_time').attr('required');
    } else {
        $('#schedule_zone').addClass('d-none');
        $('#schedule_time').removeAttr('required');
    }
});

$('[data-switch=true]').bootstrapSwitch();

var inputElm = document.querySelector('.numbers'),
    tagify = new Tagify(inputElm);

function validateFormNow(form_id) {
    return FormValidation.formValidation(
        document.getElementById(form_id),
        {
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Campaign name is required'
                        },
                        stringLength: {
                            min: 2,
                            message: 'The campaign name at least contains 02 characters'
                        }
                    }
                },
                message: {
                    validators: {
                        notEmpty: {
                            message: 'Message is required'
                        },
                    }
                },
                sender_id: {
                    validators: {
                        notEmpty: {
                            message: 'Sender ID is required'
                        },
                    }
                }
            },

            plugins: {
                declarative: new FormValidation.plugins.Declarative({
                    html5Input: true,
                }),
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
            }
        }
    );
}

function sendMsg() {

    var isChecked = document.getElementById("schedule-switch").checked;

    if (isChecked) {
        var nowDate = moment(new Date()).format('yyyy-MM-D hh:mm:ss');
        var sch_time = moment($('#schedule_time').val()).format('yyyy-MM-D hh:mm:ss');


        if (sch_time == null || sch_time == 'undefined' || sch_time == 'Invalid date') {
            Notifications.showErrorMsg('Invalid Schedule Date Time');
            return;
        }

        if (nowDate >= sch_time) {
            Notifications.showErrorMsg('Invalid Schedule Date Time');
            return;
        }
    }


    form_id = '#createForm';
    let url = $(form_id).attr('action');
    let method = $(form_id).attr('method');

    validateFormNow('createForm').validate().then(function (status) {
        if (status == "Valid") {
            $(form_id).ajaxSubmit(
                {
                    clearForm: true,
                    url: url,
                    type: method,
                    success: function (result) {
                        if (result.success) {
                            Notifications.showSuccessMsg(result.message);
                            location.reload();
                        } else {
                            Notifications.showErrorMsg(result.message);
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        Notifications.showErrorMsg(errorThrown);
                    }
                }
            );
        }
    })
}

function getInfo() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    validateFormNow('createForm').validate().then(function (status) {
        if (status == "Valid") {
            let fd = new FormData();

            fd.append('numbers', $('#numbers').val());
            fd.append('contacts', $('#contacts').val());
            fd.append('group', $('#groups').val());
            fd.append('csv_file', $('#csv_file')[0].files[0]);
            fd.append('message', $('#message').val());
            fd.append('business_id', $('#business_id').val());

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{route('sms-campaigns.info')}}",
                type: "post",
                dataType: 'json',
                processData: false, // important
                contentType: false, // important
                data: fd,
                success: function (result) {
                    let data = result.data;
                    swalWithBootstrapButtons.fire({
                        title: 'Are you sure?',
                        html: data.msg,
                        footer: 'Campaign charge and sms count can be different than this.',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, send it!',
                        cancelButtonText: 'No, cancel!',
                        reverseButtons: true
                    }).then((result) => {
                        if (result.isConfirmed) {
                            sendMsg()
                            swalWithBootstrapButtons.fire(
                                'Sent!',
                                'Your campaign start running.',
                                'success'
                            )
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire(
                                'Cancelled',
                                'Campaign Cancelled',
                                'error'
                            )
                        }
                    })

                }
            });

        }

    });
}
