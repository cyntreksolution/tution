gsm7bitChars = "@£$¥èéùìòÇ\\nØø\\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\\\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà";
gsm7bitExChar = "\\^{}\\\\\\[~\\]|€";
gsm7bitRegExp = RegExp("^[" + SmsCounter.gsm7bitChars + "]*$");
gsm7bitExRegExp = RegExp("^[" + SmsCounter.gsm7bitChars + SmsCounter.gsm7bitExChar + "]*$");
gsm7bitExOnlyRegExp = RegExp("^[\\" + SmsCounter.gsm7bitExChar + "]*$");
GSM_7BIT = "GSM_7BIT";
GSM_7BIT_EX = "GSM_7BIT_EX";
UTF16 = "UTF16";
messageLength = {GSM_7BIT: 160, GSM_7BIT_EX: 160, UTF16: 70};
multiMessageLength = {GSM_7BIT: 153, GSM_7BIT_EX: 153, UTF16: 67};


let emo = $("#message").emojioneArea({
    events: {
        keyup: function (editor, event) {
            countChar(this);
        },
        change: function (editor, event) {
            countChar(this);
        },
        emojibtn_click: function (editor, event) {
            countChar(this);
        }
    }
});


function countChar(val) {
    text = val.getText();
    var count, encoding, length, messages, per_message, remaining;
    encoding = this.detectEncoding(text);
    length = text.length;
    if (encoding === this.GSM_7BIT_EX) {
        length += this.countGsm7bitEx(text)
    }
    per_message = this.messageLength[encoding];
    if (length > per_message) {
        per_message = this.multiMessageLength[encoding]
    }
    messages = Math.ceil(length / per_message);
    remaining = per_message * messages - length;
    if (remaining == 0 && messages == 0) {
        remaining = per_message;
    }
    count = {
        encoding: encoding,
        length: length,
        per_message: per_message,
        remaining: remaining,
        messages: messages
    }
    target = $('#sms-counter');
    _results = [];
    for (k in count) {
        v = count[k];
        _results.push(target.find("." + k).text(v))
    }
    return _results
}

detectEncoding = function (text) {
    switch (false) {
        case text.match(this.gsm7bitRegExp) == null:
            return this.GSM_7BIT;
        case text.match(this.gsm7bitExRegExp) == null:
            return this.GSM_7BIT_EX;
        default:
            return this.UTF16
    }
}

countGsm7bitEx = function (text) {
    var char2, chars;
    chars = function () {
        var _i, _len, _results;
        _results = [];
        for (_i = 0, _len = text.length; _i < _len; _i++) {
            char2 = text[_i];
            if (char2.match(this.gsm7bitExOnlyRegExp) != null) {
                _results.push(char2)
            }
        }
        return _results
    }.call(this);
    return chars.length
}


function validateFormNow(form_id) {
    return  fv = FormValidation.formValidation(
        document.getElementById(form_id),
        {
            fields: {
                message: {
                    validators: {
                        notEmpty: {
                            message: 'Message is required'
                        },
                    }
                },
                sender_id: {
                    validators: {
                        notEmpty: {
                            message: 'Sender ID is required'
                        },
                    }
                },
                number: {
                    validators: {
                        stringLength: {
                            min:9,
                            max: 11,
                            message: 'number should  equal or less than 11 digits and more than or equal to 9 '
                        },
                    }
                }
            },
            plugins: {
                declarative: new FormValidation.plugins.Declarative({
                    html5Input: true,
                }),
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap(),
            }
        }
    );
}

function sendMsg() {
    form_id = '#createForm';
    let url = $(form_id).attr('action');
    let method = $(form_id).attr('method');
    validateFormNow('createForm').validate().then(function (status) {
        if (status == "Valid") {
            $(form_id).ajaxSubmit(
                {
                    clearForm: true,
                    url: url,
                    type: method,
                    success: function (result) {
                        if (result.success) {
                            Notifications.showSuccessMsg(result.message);
                        } else {
                            Notifications.showErrorMsg(result.message);
                        }

                        $("#sender_id").val("");
                        $("#contact").val("");
                        $('sender_id').val('');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        Notifications.showErrorMsg(errorThrown);
                        $('sender_id').val('');
                    }
                }
            );


        }

    })
}
