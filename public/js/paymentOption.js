$(function () {
    $(".bankTransfer").click(function () {
        if ($(this).is(":checked")) {
            $("#div_transfer").show();
            $("#div_deposit").hide();
            $("#formButton").hide();
        } else {
            $("#div_transfer").hide();
            // $("#div_deposit").show();
        }
    });
    $("#div_deposit").hide();
    $("#formButton").hide();
});
$(function () {
    $(".bankDeposit").click(function () {
        if ($(this).is(":checked")) {
            $("#div_deposit").show();
            $("#formButton").show();
            $("#div_transfer").hide();

        } else {
            $("#div_deposit").hide();
        }
    });
});

PaymentOption = {
    initPaymentOption:function(merchantId, user, invoice, apiKey,product_name) {
var user = (JSON.parse(user));
var invoice = (JSON.parse(invoice));

        DirectPayCardPayment.init({
            container: 'card_container', //<div id="card_container"></div>
            merchantId: merchantId, //your merchant_id IC02088 IC07920 IC02088
            amount: invoice.amount,
            refCode: invoice.invoice_no, //unique referance code form merchant
            currency: 'LKR',
            type: 'ONE_TIME_PAYMENT',
            customerEmail: user.email,
            customerMobile: user.mobile,
            description: product_name,  //product or service description
            debug: true,
            responseCallback: responseCallback,
            errorCallback: errorCallback,
            logo: 'https://test.com/directpay_logo.png',
            apiKey: apiKey, //7aa777f1671eb78e56395e7bf275c6e9012faee3fac8dcb34b2f549425b2982e a2a9085d5c669ad6b7dd7aa736eb2b570757cfb9c03aa7d7b49f9c377b42416d
        });

        //response callback.
        function responseCallback(result) {
            var data = {result: result};
            $.ajax({
                type: "POST",
                url: '/sender/payment-response',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if (result.success) {
                    var table = $('#datatable').DataTable();
                    table.ajax.reload();
                    Notifications.showSuccessMsg(result.message);
                    } else {
                        Notifications.showErrorMsg(result.message);
                    }
                }
            });
            // console.log("successCallback-Client", result);
            // alert(JSON.stringify(result));
        }

        //error callback
        function errorCallback(result) {
            var data = {result: result};
            $.ajax({
                type: "POST",
                url: '/sender/payment-error-response',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if (result.success) {
                        var table = $('#datatable').DataTable();
                        table.ajax.reload();
                        Notifications.showErrorMsg(result.message);
                    } else {
                        Notifications.showErrorMsg(result.message);
                    }
                }
            });
            // console.log("successCallback-Client", result);
             alert(JSON.stringify(result));
        }
    },


}

